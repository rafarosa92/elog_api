create table tb_user (
	id int identity(1,1),
	str_login varchar(500),
	str_name varchar(500),
	str_email varchar(300),
	str_password varchar(300),
	int_profile_type int,
	dt_last_access datetime,
	id_insert_by int,
	id_update_by int,
	dt_insert datetime,
	dt_update datetime,
	ind_active bit
);

alter table tb_user add constraint pk_user primary key (id);
alter table tb_user add constraint fk_user_insertby foreign key (id_insert_by) references tb_user(id);
alter table tb_user add constraint fk_user_updateby foreign key (id_update_by) references tb_user(id);
GO