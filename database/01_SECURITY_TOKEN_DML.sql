create table tb_security_token (
	id int identity(1,1),
	str_username varchar(100),
	str_password varchar(100),
	str_token varchar(300),
	dt_token_expiration datetime,
	dt_last_access datetime,
	id_insert_by int,
	id_update_by int,
	dt_insert datetime,
	dt_update datetime,
	ind_active bit
);

alter table tb_security_token add constraint pk_securitytoken primary key (id);
alter table tb_security_token add constraint fk_securitytoken_insertby foreign key (id_insert_by) references tb_user(id);
alter table tb_security_token add constraint fk_securitytoken_updateby foreign key (id_update_by) references tb_user(id);
GO

insert into tb_security_token (str_username, str_password, str_token, dt_token_expiration, dt_last_access, id_insert_by, id_update_by, dt_insert, dt_update, ind_active)
values('admin', '89794B621A313BB59EED0D9F0F4E8205', '', null, null, 1 , 1, getdate(), getdate(), 1)