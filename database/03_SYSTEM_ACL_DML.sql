create table tb_system_acl (
	id int identity(1,1),
	id_system int,
	str_ip_allowed varchar(200),
	id_insert_by int,
	id_update_by int,
	dt_insert datetime,
	dt_update datetime,
	ind_active bit
);

alter table tb_system_acl add constraint pk_systemacl primary key (id);
alter table tb_system_acl add constraint fk_systemacl_insertby foreign key (id_insert_by) references tb_user(id);
alter table tb_system_acl add constraint fk_systemacl_updateby foreign key (id_update_by) references tb_user(id);
GO