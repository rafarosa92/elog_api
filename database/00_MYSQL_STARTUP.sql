
create table api.tb_security_token (
	id int auto_increment primary key,
	str_username varchar(100) NULL,
	str_password varchar(100) NULL,
	str_token varchar(300) NULL,
	dt_token_expiration datetime NULL,
	dt_last_access datetime NULL,
	id_insert_by int NULL,
	id_update_by int NULL,
	dt_insert datetime NULL,
	dt_update datetime NULL,
	ind_active bit NULL
);

create table api.tb_security_system (
	id int auto_increment primary key,
	str_api_key varchar(200) NULL,
	str_system_name varchar(200) NULL,
	str_system_url varchar(200) NULL,
	id_insert_by int NULL,
	id_update_by int NULL,
	dt_insert datetime NULL,
	dt_update datetime NULL,
	ind_active bit NULL
);

create table api.tb_security_system_acl (
	id int auto_increment primary key,
	id_system int NULL,
	str_ip_allowed varchar(200) NULL,
	id_insert_by int NULL,
	id_update_by int NULL,
	dt_insert datetime NULL,
	dt_update datetime NULL,
	ind_active bit NULL
);

create table api.tb_user(
	id int auto_increment primary key,
	str_login varchar(500) NULL,
	str_name varchar(500) NULL,
	str_email varchar(300) NULL,
	str_password varchar(300) NULL,
	int_profile_type int NULL,
	dt_last_access datetime NULL,
	id_insert_by int NULL,
	id_update_by int NULL,
	dt_insert datetime NULL,
	dt_update datetime NULL,
	ind_active bit NULL
);


select * from api.tb_user;
select * from api.tb_security_token;
select * from api.tb_security_system;

insert into api.tb_security_system (str_api_key, str_system_name, str_system_url, id_insert_by, id_update_by, dt_insert, dt_update, ind_active)
values ('hhadiu ashduiashd', 'SystemName', '/api/user/authentication', null, null, now(), now(), 1);

insert into api.tb_security_system_acl (id_system, str_ip_allowed, id_insert_by, id_update_by, dt_insert, dt_update, ind_active)
values (1, '::1', 1, 1, now(), now(), 1);

insert into api.tb_user(str_login, str_name, str_email, str_password, int_profile_type, dt_last_access, id_insert_by, id_update_by, dt_insert, dt_update, ind_active)
values ('admin', 'Elog API', 'renan.basso@mblabs.com.br', '89794B621A313BB59EED0D9F0F4E8205', 1, now(), null, null, now(), now(), 1);

insert into api.tb_security_token (str_username, str_password, str_token, dt_token_expiration, dt_last_access, id_insert_by, id_update_by, dt_insert, dt_update, ind_active)
values ('admin', '89794B621A313BB59EED0D9F0F4E8205', 'xaxa', now(), now(), 1, 1, now(), now(), 1);