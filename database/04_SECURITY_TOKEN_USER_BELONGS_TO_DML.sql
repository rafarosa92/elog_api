ALTER TABLE tb_security_token DROP COLUMN str_username;
ALTER TABLE tb_security_token DROP COLUMN str_password;
ALTER TABLE tb_security_token ADD COLUMN str_ip VARCHAR(50);
ALTER TABLE tb_security_token ADD COLUMN id_user INT;

CREATE TABLE tb_user_belongs_to(
	id INT auto_increment PRIMARY KEY,
	id_user INT,
	id_adm INT,
	id_insert_by INT NULL,
	id_update_by INT NULL,
	dt_insert DATETIME NULL,
	dt_update DATETIME NULL,
	ind_active BIT NULL
);
