create table tb_system (
	id int identity(1,1),
	str_api_key varchar(200),
	str_system_name varchar(200),
	str_system_url varchar(200),
	id_insert_by int,
	id_update_by int,
	dt_insert datetime,
	dt_update datetime,
	ind_active bit
);

alter table tb_system add constraint pk_system primary key (id);
alter table tb_system add constraint fk_system_insertby foreign key (id_insert_by) references tb_user(id);
alter table tb_system add constraint fk_system_updateby foreign key (id_update_by) references tb_user(id);
GO