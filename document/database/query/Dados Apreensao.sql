SELECT a.doc_id,
       a.apr_nr_doc,
       a.apr_data,
       b.ass_nome,
       sum(apr_qtde) AS volumes
FROM   tab_apreensao a
       JOIN tab_assinatura b ON b.ass_id = a.ass_id
WHERE  latu_lote='201600002258'
GROUP BY a.doc_id,
       a.apr_nr_doc,
       a.apr_data,
       b.ass_nome