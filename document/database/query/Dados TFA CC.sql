SELECT a.cnt_id,
       b.ter_id,
       d.pcnt_descricao,
       f.ava_descricao
FROM   rel_dent_cnt a
       JOIN tab_termo_cnt b ON b.mcnt_id=a.mcnt_id
       JOIN tab_ava_parte_cnt c ON c.ter_id = b.ter_id
       JOIN tab_parte_cnt d ON d.pcnt_id = c.pcnt_id
       JOIN tab_ava_parte_cnt_item e ON e.avpc_id = c.avpc_id
       JOIN tab_avarias f ON f.ava_id=e.ava_id
WHERE  a.lote_id IN ('201700009001','201700008973','201700008994','201700009019','201700018995') AND
       a.cnt_id='TCNU6982261'