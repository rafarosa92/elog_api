SELECT a.dent_id,
       c.doc_id,
       
       CASE WHEN c.doc_id LIKE '%DTA%' THEN substring(a.dent_id,3,2)+'/'+substring(a.dent_id,14,7)+'-'+substring(a.dent_id,21,1)
       ELSE substring(a.dent_id,9,13) END AS numero,
       
       CASE WHEN c.doc_id <>'NF' THEN NULL
       ELSE substring(a.dent_id,6,3) END AS serie,
       
       b.dent_dt_emissao
FROM   rel_dent_cnt a
       JOIN tab_doc_entrada b ON b.dent_id = a.dent_id
       JOIN tab_documentos c ON c.doc_ordem1=substring(a.dent_id,5,1)
WHERE  a.lote_id IN ('201700009001','201700008973','201700008994','201700009019')
       
-- select top 100 * from tab_doc_entrada order by dent_id desc
/*

201730000000002590876FOZ999999
201730000000002604885VCP999999
AAAADSSSNNNNNNNNNNNNN
123456789.123456789.123456789.
*/