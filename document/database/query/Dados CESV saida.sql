SELECT DISTINCT c.cesv_id,
       c.vei_id,
       d.vei_id AS vei_id_rbq,
       c.cesv_dt_entrada,
       c.cesv_dt_saida,
       e.cnt_id
FROM   tab_di_item a
       JOIN rel_cesv_di b ON b.dsai_id=a.dsai_id
       JOIN tab_cesv c ON c.cesv_id = b.cesv_id
       JOIN rel_cesv_reboque d ON d.cesv_id = c.cesv_id
       LEFT JOIN tab_mov_cnt e ON e.cesv_id_sai = c.cesv_id
WHERE  a.latu_lote IN ('201700008167','201700007714','201700008118','201700008156') AND
       c.cesv_cancelada IS NULL