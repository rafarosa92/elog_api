SELECT a.ter_id,
       c.tas_qtd,
       e.ava_descricao
FROM   tab_termo_lote a
       JOIN tab_termo_item b ON b.ter_id = a.ter_id
       JOIN tab_avaria_seq c ON c.ter_id = b.ter_id AND c.oitem_id = b.oitem_id
       JOIN tab_ava_seq_item d ON d.tas_id = c.tas_id AND d.ter_id = c.ter_id AND d.oitem_id = c.oitem_id
       JOIN tab_avarias e ON e.ava_id=d.ava_id
WHERE  a.lote_id IN ('201700009001','201700008973','201700008994','201700009019','201700018995') AND
       a.ter_cancelado IS NULL
