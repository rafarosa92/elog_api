SELECT distinct a.lote_id,
       a.lote_cancelado,
       a.lote_conhec,
       CASE WHEN substring(a.dent_id,22,3) = 'NAC' THEN 'NACIONAL'
          WHEN c.proc_tipo = 'T' THEN 'RODOVI�RIO'
          WHEN c.proc_tipo = 'A' THEN 'A�REO'
          WHEN c.proc_tipo = 'M' THEN 'MAR�TIMO'
          ELSE '?' END AS modal,
       d.reg_nome,
       a.lote_dt_conhec,
       b.cli_nome,
       b.cli_cgc,
       substring(a.lote_pcarga,3,36) AS lote_pcarga,
       a.lote_manifesto,
       c.proc_nome,
       a.lote_dt_venc,
       a.lote_peso_bruto,
       a.lote_qt_tot,
       l.pais_nome,
       f.cli_nome,
       h.nav_n2,
       i.porto_nome,
       CASE a.lote_tipo
          WHEN 'M' THEN 'MASTER'
          WHEN 'S' THEN 'SUBMASTER'
          WHEN 'F' THEN 'FILHOTE'
          ELSE '?' END AS lote_tipo,
       k.cli_nome AS consolidador,
       m.cp_descricao,
       (select min(litem_descricao) from tab_lote_item x where x.lote_id=a.lote_id) as descricao
FROM   tab_lote a
       JOIN tab_clientes b ON b.cli_id = a.ben_id
       JOIN tab_procedencia c ON c.proc_id=substring(a.dent_id,22,3)
       JOIN tab_regimes d ON d.reg_id = a.reg_id
       left JOIN tab_clientes f ON f.cli_id=a.lote_exportador
       LEFT JOIN tab_lote_complemento g ON g.lote_id = a.lote_id
       LEFT JOIN tab_navios h ON h.nav_id=g.nav_id
       LEFT JOIN tab_porto i ON i.porto_id=g.porto_id and i.porto_pais=g.porto_pais
       LEFT JOIN tab_lote j ON j.lote_id = dbo.retorna_lote_master(a.lote_id)
       LEFT JOIN tab_clientes k ON k.cli_id = j.ben_id
       LEFT JOIN tab_pais l ON l.pais_id=g.porto_pais
       left join tab_cargas_perigosas m on m.cp_id=a.cp_id
WHERE  a.lote_id IN ('201700009001','201700008973','201700008994','201700009019')


--SELECT TOP 100 * FROM tab_lote ORDER BY lote_id desc
--select top 100 * from tab_lote_complemento order by lote_id desc
--select * from sysobjects where name like '%master%'
--select * from tab_lote_complemento where lote_id='201700009019'
--select * from tab_lote_item where lote_id='201700008973'