SELECT substring(a.dsai_id,3,2)+'/'+substring(a.dsai_id,8,7)+'-'+substring(dsai_id,15,1) AS dsai_id,
       a.dsai_dt_registro,
       a.dsai_dt_desemb,
       b.cli_nome AS beneficiario,
       b.cli_cgc AS doc_beneficiario,
       c.cli_nome AS comissaria,
       c.cli_cgc AS doc_comissaria,
       a.dsai_cif_us,
       a.dsai_cif_us * d.cot_valor1 AS cif_br,
       a.dsai_dt_registro AS data_cotacao,
       d.cot_valor1,
       CASE a.dsai_canal
          WHEN '0' THEN 'VERDE'
          WHEN '1' THEN 'AMARELO'
          WHEN '2' THEN 'VERMELHO'
          WHEN '3' THEN 'LARANJA'
          WHEN '4' THEN 'CINZA'
          ELSE '-' END AS canal,
       (SELECT count(*) FROM tab_adi_item x WHERE x.dsai_id=a.dsai_id) AS qtd_itens
FROM   tab_doc_saida a
       JOIN tab_clientes b ON b.cli_id = a.ben_id
       JOIN tab_clientes c ON c.cli_id = a.com_id
       JOIN tab_cotacao d ON d.moe_id = '220' AND convert(DATE,d.cot_data) = convert(DATE,a.dsai_dt_registro)
WHERE  a.dsai_loteda IN ('201700008167','201700007714','201700008118','201700008156')

--SELECT TOP 100 * FROM tab_doc_saida WHERE dsai_loteda IS NOT NULL ORDER BY dsai_dt_registro desc