SELECT DISTINCT c.doc_id,
       substring(b.dsai_id,3,2)+'/'+substring(b.dsai_id,7,7)+'-'+substring(b.dsai_id,14,1) AS dsai_id,
       b.dsai_dt_registro,
       b.dsai_dt_desemb,
       d.cli_nome
FROM   tab_di_item a
       JOIN tab_doc_saida b ON b.dsai_id = a.dsai_id
       JOIN tab_documentos c ON c.doc_ordem1 = substring(b.dsai_id,5,1)
       JOIN tab_clientes d ON d.cli_id = b.com_id
WHERE  a.latu_lote IN ('201700008167','201700007714','201700008118','201700008156')