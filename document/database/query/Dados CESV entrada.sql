SELECT c.cesv_id,
       c.vei_id,
       d.vei_id,
       c.cesv_dt_entrada,
       c.cesv_dt_saida,
       a.cnt_id
FROM   rel_dent_cnt a
       JOIN rel_cesv_dent b ON b.dent_id=a.dent_id
       JOIN tab_cesv c ON c.cesv_id = b.cesv_id 
       JOIN rel_cesv_reboque d ON d.cesv_id = c.cesv_id
WHERE  a.lote_id IN ('201700009001','201700008973','201700008994','201700009019') AND
       c.cesv_cancelada IS NULL
