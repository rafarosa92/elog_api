-- Para interior, rodar o script abaixo

SELECT e.cesv_id,
       b.cnt_id,
       b.cnt_tipo,
       b.cnt_tara,
       c.tiso_descricao,
       d.mcnt_dt_desova,
       d.mcnt_dt_ova,
       e.litem_peso_bruto AS manifestado,
       f.bal_peso AS peso_entrada,
       g.bal_peso AS peso_saida,
       f.bal_peso-g.bal_peso AS aferido,
       (f.bal_peso-g.bal_peso)-e.litem_peso_bruto AS diferenca_mesmo_vei,
       ((f.bal_peso-g.bal_peso)-e.litem_peso_bruto)/(f.bal_peso-g.bal_peso)*100 AS porc_dif_mesmo_vei,
       (f.bal_peso-g.bal_peso-b.cnt_tara)-e.litem_peso_bruto AS diferenca_baixa,
       ((f.bal_peso-g.bal_peso-b.cnt_tara)-e.litem_peso_bruto)/((f.bal_peso-g.bal_peso)-b.cnt_tara)*100 AS porc_dif_baixa,
       CASE WHEN d.cesv_id_ent=cesv_id_sai THEN 'S' ELSE 'N' END AS saiu_mesmo_vei,
       h.tmc_descricao,
       case when d.srd_id is null then 'N' else 'S' end as sobre_rodas,
       case when i.esp_id='CC' then 'S' else 'N' end as baixado
FROM   rel_dent_cnt a
       JOIN tab_container b ON b.cnt_id = a.cnt_id
       JOIN tab_tipo_iso c ON c.tiso_id = b.tiso_id
       JOIN tab_mov_cnt d ON d.mcnt_id = a.mcnt_id
       JOIN tab_lote_item e ON e.lote_id=a.lote_id AND e.cnt_id=a.cnt_id
       LEFT JOIN tab_balanca f ON f.cesv_id=e.cesv_id AND f.bal_ordem=1
       LEFT JOIN tab_balanca g ON g.cesv_id=e.cesv_id AND g.bal_ordem=2
       left join tab_modalidade_container h on h.tmc_id = d.tmc_id
       left join tab_os_item i on i.cnt_id = a.cnt_id
       left join tab_os j on j.os_id=i.os_id and j.os_cancelada is NULL and j.os_tipocd='D'
WHERE  a.lote_id IN ('201700008164','201700008163','201700008162')
       and e.litem_qt_vol_dec>0
-- Para Santos, rodar a SP proc_opr_verif_peso_cnt

--SELECT * FROM tab_lote_item WHERE cnt_id='TCLU7144610'
--select lote_conhec from tab_lote where lote_id='201700008099'
--select * from tab_lote where lote_id='201700008099'
--select * from tab_lote_complemento where lote_id='201700008099'
--exec proc_opr_verif_peso_cnt '2017V0005999', 'TCLU7144610'
--select * from tab_balanca where cesv_id='2017V0005999'
--select * from tab_cesv where cesv_id='2017V0005999'
