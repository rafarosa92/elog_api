﻿using MBLabs.WMSLib.Util;

namespace MBLabs.WMSLib.Enumerator
{
    public enum ProfileType
    {
        [EnumDescription("Administrador")]
        ADMINISTRATOR = 1,

        [EnumDescription("Gerente")]
        MANAGER = 2,

        [EnumDescription("Operador")]
        OPERATOR = 3,

        [EnumDescription("Promotor")]
        PROMOTOR = 4,
    }
}
