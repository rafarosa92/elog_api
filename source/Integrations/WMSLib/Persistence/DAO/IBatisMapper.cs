﻿using System;
using System.IO;
using System.Reflection;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;
using IBatisNet.DataMapper.SessionStore;

namespace MBLabs.WMSLib.Persistence.DAO
{
    /// <summary>
    /// This class IBatis Mapper factory which retrieves a desired
    /// ibatis mapper for a certain database.
    /// </summary>
    internal class IBatisMapper
    {
        // cache for ibatis mapper
        private static ISqlMapper mapperCache = null;

        #region Private Construtor
        /// <summary>
        /// Private constructor
        /// </summary>
        private IBatisMapper()
        {
        }
        #endregion

        /// <summary>
        /// Get mapper path and file name according the given ibatis mapper type
        /// </summary>
        /// <returns>SQL Mapper</returns>
        public static ISqlMapper GetMapper()
        {
            String mapperPathAndName = "MBLabs.WMSLib.Configuration.IBatis.SqlMap.config";

            if (mapperCache == null)
            {
                // initialize ibatis mapper
                DomSqlMapBuilder builder = new DomSqlMapBuilder();

                // get a reference to the current assembly
                Assembly assembly = Assembly.GetExecutingAssembly();
                Stream stream = assembly.GetManifestResourceStream(mapperPathAndName);
                mapperCache = builder.Configure(stream);

                // set session store to hybrid, this is used to run all querys in a single ibatis thread
                mapperCache.SessionStore = new HybridWebThreadSessionStore(mapperCache.Id);
            }

            return mapperCache;
        }
    }
}