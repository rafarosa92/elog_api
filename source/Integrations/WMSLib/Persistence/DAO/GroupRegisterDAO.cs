﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.WMSLib.Exception;
using MBLabs.WMSLib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.WMSLib.Persistence.DAO
{
    public class GroupRegisterDAO : DAOManager<GroupRegister>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public IList<GroupRegister> SelectByArtikelId(String artikelId)
        {
            _log.Info("Begin Method");

            IList<GroupRegister> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<GroupRegister>("GroupRegister.SelectByArtikelId", artikelId);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        #endregion
    }
}