﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Persistence.DAO
{
    public class Averbacao_DIDAO 
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public IList<Averbacao_DI> getAll()
        {
            _log.Info("Begin Method");

            IList<Averbacao_DI> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapperSqlServer();
                returnValue = mapper.QueryForList<Averbacao_DI>("Averbacao_DI.SelectAll", null);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        #endregion
    }
}