﻿using System;

namespace MBLabs.WMSLib.Transfer
{
    public class GroupRegister
    {
        public String ArtikelId { get; set; }
        public String Group { get; set; }
    }
}