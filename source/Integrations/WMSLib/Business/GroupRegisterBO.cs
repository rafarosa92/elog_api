﻿using log4net;
using MBLabs.WMSLib.Exception;
using MBLabs.WMSLib.Persistence.DAO;
using MBLabs.WMSLib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.WMSLib.Business
{
    public class GroupRegisterBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public IList<GroupRegister> GetById(String artikelId)
        {
            _log.Info("Begin Method");

            IList<GroupRegister> returnValue;

            try
            {
                GroupRegisterDAO groupRegisterDAO = new GroupRegisterDAO();
                returnValue = groupRegisterDAO.SelectByArtikelId(artikelId);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}