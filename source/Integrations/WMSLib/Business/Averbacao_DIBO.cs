﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Business
{
    public class Averbacao_DIBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public IList<Averbacao_DI> GetAverbacaoList()
        {
            _log.Info("Begin Method");

            IList<Averbacao_DI> returnValue = null;

            try
            {
                Averbacao_DIDAO averbacao_DIDAO = new Averbacao_DIDAO();
                returnValue = averbacao_DIDAO.getAll();
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}