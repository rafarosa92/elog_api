﻿using log4net;
using MBLabs.TrackingLib.Enumerator;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Persistence.DAO;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Business
{
    public class UserBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public User Authenticate(String username, String password)
        {
            _log.Info("Begin Method");

            User returnValue;

            try
            {
                // perform search user by username
                UserAccessProfileDAO userDAO = new UserAccessProfileDAO();
                returnValue = userDAO.SelectByLogin(username);

                if (returnValue != null)
                {
                    if (returnValue.Password.ToUpper() != password.ToUpper())
                    {
                        throw new AuthenticationFailureException("Password invalid");
                    }

                    if (returnValue.DtExpiration <= DateTime.Now)
                    {
                        throw new AuthenticationFailureException("User was expirated");
                    }

                    if (returnValue.IsBlocked.ToUpper() == "S")
                    {
                        throw new AuthenticationFailureException("User was blocked");
                    }
                }
                else
                {
                    throw new AuthenticationFailureException("User not found");
                }
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public User Save(User user)
        {
            _log.Info("Begin Method");

            User returnValue = null;

            try
            {
                if (user.P_operation == "I" || user.P_operation == "S")
                {
                    UserAccessProfileDAO userDAO = new UserAccessProfileDAO();
                    returnValue = userDAO.Save(user);
                }
                

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public User SelectByEmail(String email)
        {
            _log.Info("Begin Method");

            User returnValue = null;

            try
            {
                UserAccessProfileDAO userDAO = new UserAccessProfileDAO();
                returnValue = userDAO.SelectByEmail(email);


            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public User SelectByLogin(String login)
        {
            _log.Info("Begin Method");

            User returnValue = null;

            try
            {
                UserAccessProfileDAO userDAO = new UserAccessProfileDAO();
                returnValue = userDAO.SelectByLogin(login);


            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}