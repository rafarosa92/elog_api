﻿using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Persistence.DAO;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Business
{
    public class CompanyBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public IList<Company> GetCompanyList()
        {
            _log.Info("Begin Method");

            IList<Company> returnValue = null;

            try
            {
                CompanyDAO companyDAO = new CompanyDAO();
                returnValue = companyDAO.SelectByNullStatus();
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public Company GetCompanyListNotNull()
        {
            _log.Info("Begin Method");

            Company returnValue = null;

            try
            {
                CompanyDAO companyDAO = new CompanyDAO();
                returnValue = companyDAO.SelectByNotNullStatus();
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public Company GetCompanyByName(String companyName)
        {
            _log.Info("Begin Method");

            Company returnValue = null;

            try
            {
                CompanyDAO companyDAO = new CompanyDAO();
                returnValue = companyDAO.SelectByName(companyName);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList <Company> GetCompanyByType(String type)
        {
            _log.Info("Begin Method");

            IList<Company> returnValue = null;

            try
            {
                CompanyDAO companyDAO = new CompanyDAO();
                returnValue = companyDAO.SelectByType(type);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Company> GetCompanyByLogin(Int32 login)
        {
            _log.Info("Begin Method");

            IList<Company> returnValue = null;

            try
            {
                CompanyDAO companyDAO = new CompanyDAO();
                returnValue = companyDAO.SelectByLogin(login);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Company> GetCompanyById(Int32 Id)
        {
            _log.Info("Begin Method");

            IList<Company> returnValue = null;

            try
            {
                CompanyDAO companyDAO = new CompanyDAO();
                returnValue = companyDAO.SelectCompanyById(Id);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}