﻿using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Persistence.DAO;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Business
{
    public class ParametersBO : DAOManager<Parameters>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public IList<Parameters> GetParametersList()
        {
            _log.Info("Begin Method");

            IList<Parameters> returnValue = null;

            try
            {
                ParametersDAO parametersDAO = new ParametersDAO();
                returnValue = parametersDAO.getParameters();
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Parameters> GetParamByDesc(String parameters)
        {
            _log.Info("Begin Method");

            IList<Parameters> returnValue = null;

            try
            {
                ParametersDAO parametersDAO = new ParametersDAO();
                returnValue = parametersDAO.getByParamDesc(parameters);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<ParametersCnpj> GetParamCnpjById(Int32 Id)
        {
            _log.Info("Begin Method");

            IList<ParametersCnpj> returnValue = null;

            try
            {
                ParametersDAO parametersDAO = new ParametersDAO();
                returnValue = parametersDAO.getByParamCnpjById(Id);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public ParametersCnpj SelectConsolidadorCNPJ(Int32 Id)
        {
            _log.Info("Begin Method");

           ParametersCnpj returnValue = null;

            try
            {
                ParametersDAO parametersDAO = new ParametersDAO();
                returnValue = parametersDAO.SelectConsolidadorCNPJ(Id);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public ParametersCnpj SelectCNPJs(Int32 Id)
        {
            _log.Info("Begin Method");

            ParametersCnpj returnValue = null;

            try
            {
                ParametersDAO parametersDAO = new ParametersDAO();
                returnValue = parametersDAO.SelectCNPJs(Id);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public Parameters GetByParamDescUni(String Desc, Int32 Uni_id)
        {
            _log.Info("Begin Method");

            Parameters returnValue = null;

            try
            {

                ParametersDAO parametersDAO = new ParametersDAO();
                returnValue = parametersDAO.SelectByParamDescUni(Desc, Uni_id);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}