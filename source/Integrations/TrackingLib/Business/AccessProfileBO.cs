﻿using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Persistence.DAO;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Business
{
    public class AccessProfileBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public IList<AccessProfile> ListUserAccess(Int32 UserId)
        {
            IList<AccessProfile> returnValue = null;
            try
            {
                AccessProfileDAO accessProfileDAO = new AccessProfileDAO();
                returnValue = accessProfileDAO.SelectByUser(UserId);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }
            return returnValue;
        }
    }
}