﻿using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Persistence.DAO;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Business
{
    public class MenuBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public IList<Menu> GetMenuList()
        {
            _log.Info("Begin Method");

            IList<Menu> returnValue = null;

            try
            {
                MenuDAO MenuDAO = new MenuDAO();
                returnValue = MenuDAO.getMenu();
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Menu> getMenuById(Int32 usu_id)
        {
            _log.Info("Begin Method");

            IList<Menu> returnValue = null;

            try
            {
                MenuDAO MenuDAO = new MenuDAO();
                returnValue = MenuDAO.getMenuById(usu_id);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

    }
}