﻿using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Persistence.DAO;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Business
{
    public class HistoricoBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public IList<Historico> InsertHist(Int32 EV_ID, Int32 USU_ID_OPERADOR, Int32 USU_ID_AFETADO, String hs_obs, Int32 acesso_id)
        {
            _log.Info("Begin Method");

            IList<Historico> returnValue = null;

            try
            {
                Historico historico = new Historico();
                historico.EV_ID = EV_ID;
                historico.USU_ID_OPERADOR = USU_ID_OPERADOR;
                historico.USU_ID_AFETADO = USU_ID_AFETADO;
                historico.hs_obs = hs_obs;
                historico.acesso_id = acesso_id;
                HistoricoDAO historicoDAO = new HistoricoDAO();
                returnValue = historicoDAO.Insert(historico);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}