﻿using System;

namespace MBLabs.TrackingLib.Exception
{
    [Serializable]
    public class BusinessException : ApplicationException
    {
        public BusinessException(System.Exception innerExc) : base(innerExc.GetType().Name, innerExc)
        {
        }

        public BusinessException(String innerExc) : base(innerExc)
        {
        }
    }
}