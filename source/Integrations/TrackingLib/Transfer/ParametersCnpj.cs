﻿using System;

namespace MBLabs.TrackingLib.Transfer
{ 

    public class ParametersCnpj
    {
        public String NomeCliente { get; set; }
        public String CnpjCliente { get; set; }
        public Int32 TipoCliente { get; set; }
        public String DescricaoCliente { get; set; }
        public String beneficiario { get; set; }
        public String comissaria { get; set; }
    }
}