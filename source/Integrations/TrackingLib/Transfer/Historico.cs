﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.TrackingLib.Transfer
{
    public class Historico
    {
        public Int32 HS_ID { get; set; }
        public Int32 EV_ID { get; set; }
        public Int32 USU_ID_OPERADOR { get; set; }
        public Int32 USU_ID_AFETADO { get; set; }
        public DateTime HS_DATA { get; set; }
        public String hs_obs { get; set; }
        public Int32 acesso_id { get; set; }
    }
}
