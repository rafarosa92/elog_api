﻿using System;

namespace MBLabs.TrackingLib.Transfer
{
    public class User
    {
        public Int32 Id { get; set; }
        public String Login { get; set; }
        public String Token { get; set; }
        public String Name { get; set; }
        public String Company { get; set; }
        public String Phone { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }
        public String CPF { get; set; }
        public DateTime DtExpiration { get; set; }
        public String IsBlocked { get; set; }
        public String Recinto { get; set; }
        public Int32 RecintoID { get; set; }
        public String Obs { get; set; }
        public Int32 P_result { get; set; }
        public String P_operation { get; set; }
        public Int32 Id_operator { get; set; }
        

    }

   
}