﻿using System;

namespace MBLabs.TrackingLib.Transfer
{
    public class Parameters
    {
        public Int32 Id { get; set; }
        public String Description { get; set; }
        public String Value { get; set; }
        public String Observation { get; set; }
        /*tab_parametro_unidade*/
        public Int32 UNI_ID { get; set; }
        public String TPU_DESC { get; set; }
        public String TPU_VALOR { get; set; }
        

    }

    
}