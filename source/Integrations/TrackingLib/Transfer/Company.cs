﻿using System;

namespace MBLabs.TrackingLib.Transfer
{
    public class Company
    {
        public Int32 Id { get; set; }
        public Int32 UNI_COD1 { get; set; }
        public String Name { get; set; }
        public String Type { get; set; }
        public String Status { get; set; }
        public String EngName { get; set; }
        public String Keeper { get; set; }
        public String KeeperTelphone { get; set; }
        public String KeeperEmail { get; set; }
        public String RA { get; set; }
    }
}