﻿using System;

namespace MBLabs.TrackingLib.Transfer
{
    public class AccessProfile
    {
        public Int32 AccessId { get; set; }
        public Int32 ProfileAccess { get; set; }
    }
}