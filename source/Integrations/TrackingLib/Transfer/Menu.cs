﻿using System;

namespace MBLabs.TrackingLib.Transfer
{
    public class Menu
    {
        public Int32 Id { get; set; }
        public String acesso_descricao { get; set; }
        public Int32 acesso_pai { get; set; }
        public String acesso_menu { get; set; }
        public String acesso_descricao_eng { get; set; }
        public String acesso_pagina { get; set; }
        public String acesso_icone { get; set; }

    }
}