﻿using MBLabs.TrackingLib.Enumerator;

namespace MBLabs.TrackingLib.Configuration
{
    public class ServerConfiguration
    {
        /* SERVER ID IS USED IN ID_USER INSERT AND UPDATE COLUMNS ON DATABASE */
        public const int SERVER_ID = 1;
    }
}