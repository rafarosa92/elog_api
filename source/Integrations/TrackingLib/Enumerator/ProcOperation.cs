﻿namespace MBLabs.TrackingLib.Enumerator
{
    public enum ProcOperation
    {
        S,
        L,
        B,
        I,
        U
    }
}