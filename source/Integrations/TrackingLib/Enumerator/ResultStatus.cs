﻿namespace MBLabs.TrackingLib.Enumerator
{
    public enum ResultStatus
    {
        SUCCESS = 1,
        SUCCESS_WARNING = 2,
        ERROR = 3
    }
}
