﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Persistence.DAO
{
    public class CompanyDAO : DAOManager<User>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public IList<Company> SelectByNullStatus()
        {
            _log.Info("Begin Method");

            IList<Company> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<Company>("Company.SelectByNullStatus", null);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public Company SelectByNotNullStatus()
        {
            _log.Info("Begin Method");

            Company returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<Company>("Company.SelectByNotNullStatus", null);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public Company SelectByName(String companyName)
        {
            _log.Info("Begin Method");

            Company returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<Company>("Company.SelectByName", companyName);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Company> SelectByType(String type)
        {
            _log.Info("Begin Method");

            IList<Company> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<Company>("Company.SelectbyType", type);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Company> SelectByLogin(Int32 login)
        {
            _log.Info("Begin Method");

            IList<Company> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<Company>("Company.SelectbyLogin", login);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Company> SelectCompanyById(Int32 Id)
        {
            _log.Info("Begin Method");

            IList<Company> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<Company>("Company.SelectById", Id);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        #endregion
    }
}