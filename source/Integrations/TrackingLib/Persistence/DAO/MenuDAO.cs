﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Persistence.DAO
{
    public class MenuDAO : DAOManager<Menu>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public IList<Menu> getMenu()
        {
            _log.Info("Begin Method");

            IList<Menu> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<Menu>("Menu.SelectAll", null);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Menu> getMenuById(Int32 usu_id)
        {
            _log.Info("Begin Method");

            IList<Menu> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<Menu>("Menu.SelectById", usu_id);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }


        #endregion
    }
}