﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Persistence.DAO
{
    public class ParametersDAO 
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public IList<Parameters> getParameters()
        {
            _log.Info("Begin Method");

            IList<Parameters> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<Parameters>("Parameters.SelectAll", null);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Parameters> getByParamDesc(String parameters)
        {
            _log.Info("Begin Method");

            IList<Parameters> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<Parameters>("Parameters.SelectByParamDesc", parameters);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<ParametersCnpj> getByParamCnpjById(Int32 Id)
        {
            _log.Info("Begin Method");

            IList<ParametersCnpj> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<ParametersCnpj>("ParametersCnpj.SelectParamCnpjById", Id);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public ParametersCnpj SelectConsolidadorCNPJ(Int32 Id)
        {
            _log.Info("Begin Method");

            ParametersCnpj returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<ParametersCnpj>("ParametersCnpj.SelectConsolidador", Id);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public ParametersCnpj SelectCNPJs(Int32 Id)
        {
            _log.Info("Begin Method");

            ParametersCnpj returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<ParametersCnpj>("ParametersCnpj.SelectCNPJs", Id);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public Parameters SelectByParamDescUni(String Desc, Int32 Uni_Id)
        {
            _log.Info("Begin Method");

            Parameters returnValue = null;
            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["UNI_ID"] = Uni_Id.ToString();
            parameter["TPU_DESC"] = Desc;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<Parameters>("Parameters.SelectByParamDescUni", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        #endregion
    }
}
