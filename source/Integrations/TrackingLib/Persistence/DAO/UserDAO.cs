﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLib.Enumerator;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Persistence.DAO
{
    public class UserAccessProfileDAO : DAOManager<User>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public User SelectByLogin(String login)
        {
            _log.Info("Begin Method");

            User returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<User>("User.SelectByLogin", login);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public User SelectByEmail(String email)
        {
            _log.Info("Begin Method");

            User returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<User>("User.SelectByEmail", email);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        //public IList<Int32> SelectAccessByUser
        #endregion



        public User Save(User user)
        {
            _log.Info("Begin Method");

            User returnValue = null ;

            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();
                parameter["USU_ID"] = base.ConvertParamFromInt32(user.Id);
                parameter["USU_LOGIN"] = base.ConvertParamFromString(user.Login);
                parameter["USU_NOME"] = base.ConvertParamFromString(user.Name);
                parameter["USU_EMPRESA"] = base.ConvertParamFromString(user.Company);
                parameter["USU_TELEFONE"] = base.ConvertParamFromString(user.Phone);
                parameter["USU_SENHA"] = base.ConvertParamFromString(user.Password);
                parameter["USU_EMAIL"] = base.ConvertParamFromString(user.Email);
                parameter["USU_CPF"] = base.ConvertParamFromString(user.CPF);
                parameter["USU_BLOQUEADO"] = base.ConvertParamFromString(user.IsBlocked);
                parameter["USU_OBS"] = base.ConvertParamFromString(user.Obs);
                parameter["USU_ID_OPERADOR"] = base.ConvertParamFromInt32(user.Id_operator);// fica esperto aqui que eu peguei o valor do tracking pois ele ainda nao tem cadastro no sistema.
                parameter["P_UNI"] = base.ConvertParamFromInt32(user.RecintoID);
                parameter["P_OPERACAO"] = base.ConvertParamFromString(user.P_operation);


                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<User>("User.ProcUser_insert", parameter);
                _log.Info(parameter);


            }
            catch (System.Exception ex)
            {
                if (ex is UserException)
                {
                    throw ex;
                }
                else
                {
                    _log.Error(ex);
                    throw new PersistenceException(ex);
                }
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}