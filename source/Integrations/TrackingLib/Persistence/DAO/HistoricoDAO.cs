﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Persistence.DAO
{
    public class HistoricoDAO : DAOManager<Historico>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public IList<Historico> Insert(Historico historico)
        {
            _log.Info("Begin Method");

            IList<Historico> returnValue = null;

            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();
                parameter["EV_ID"] = base.ConvertParamFromInt32(historico.EV_ID);
                parameter["USU_ID_OPERADOR"] = base.ConvertParamFromInt32(historico.USU_ID_OPERADOR);
                parameter["USU_ID_AFETADO"] = base.ConvertParamFromInt32(historico.USU_ID_AFETADO);
                parameter["hs_obs"] = base.ConvertParamFromString(historico.hs_obs);
                //parameter["acesso_id"] = base.ConvertParamFromInt32(historico.acesso_id); ver com cesar a dinamica desse log  

                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<Historico>("Historico.Insert", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        #endregion
    }
}