﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLib.Exception;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.TrackingLib.Persistence.DAO
{
    public class AccessProfileDAO : DAOManager<User>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public IList<AccessProfile> SelectByUser(Int32 userID)
        {
            _log.Info("Begin Method");

            IList<AccessProfile> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<AccessProfile>("AccessProfile.SelectByuser", userID);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        //public IList<Int32> SelectAccessByUser
        #endregion
    }
}