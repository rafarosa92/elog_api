﻿using System;
using System.Collections.Generic;
using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLib.Exception;

namespace MBLabs.TrackingLib.Persistence.DAO
{
    /// <summary>
    /// Class responsible for setting all persistence generic methods
    /// </summary>
    public class DAOManager<T>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Insert Method
        /// <summary>
        /// This method is responsible for inserting into database
        /// </summary>
        /// <typeparam name="T">Generic table</typeparam>
        /// <param name="parameters">Generic parameters</param>
        public Int32 Insert(object parameters)
        {
            _log.Info("Begin Method");

            String tableName = String.Empty; Int32 returnId = 0; Object returnObject;

            try
            {
                tableName = typeof(T).Name;
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnObject = mapper.Insert(tableName + ".Insert", parameters);

                if (returnObject != null)
                {
                    returnId = (Int32)returnObject;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnId;
        }
        
        #endregion

        #region Update Method

        /// <summary>
        /// Method responsible for updating into database
        /// </summary>
        /// <typeparam name="T">Generic table</typeparam>
        /// <param name="parameters">Generic parameters</param>
        public void Update(object parameters)
        {
            _log.Info("Begin Method");

            String tableName = String.Empty; Int32 returnId = 0; Object returnObject;

            try
            {
                tableName = typeof(T).Name;
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnObject = mapper.Update(tableName + ".Update", parameters);

                if (returnObject != null)
                {
                    returnId = (Int32)returnObject;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");
        }

        #endregion

        #region Delete Method
        /// <summary>
        /// This method is responsible for deleting from database
        /// </summary>
        /// <typeparam name="T">Generic table</typeparam>
        /// <param name="parameters">Generic parameters</param>
        public void Delete(object parameters)
        {
            _log.Info("Begin Method");

            String tableName = String.Empty;

            try
            {
                tableName = typeof(T).Name;
                ISqlMapper mapper = IBatisMapper.GetMapper();
                mapper.Delete(tableName + ".Delete", parameters);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");
        }

        #endregion

        #region Select Methods

        /// <summary>
        /// Method responsible for selecting by id a entity row 
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <param name="identifier">Entity ID</param>
        /// <returns>Entity Object</returns>
        public virtual T SelectById(Int32 identifier)
        {
            _log.Info("Start Method");

            T returnValue = default(T);

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<T>(typeof(T).Name + ".SelectById", identifier);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        /// <summary>
        /// Method responsible for selecting all entity rows 
        /// </summary>
        /// <typeparam name="T">Entity type</typeparam>
        /// <returns>List of Entity Object</returns>
        public IList<T> SelectAll()
        {
            _log.Info("Start Method");

            IList<T> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<T>(typeof(T).Name + ".SelectAll", null);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        /// <summary>
        /// Method responsible for selecting user list by search parameter
        /// </summary>
        /// <param name="searchParameter">user search parameter</param>
        /// <returns>user object list</returns>
        public IList<T> SelectBySearchParameter(Object searchParameter, out Int32 countAll)
        {
            _log.Info("Begin Method");

            IList<T> returnList = null;

            try
            {
                // perform an user search
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnList = mapper.QueryForList<T>(typeof(T).Name + ".SelectBySearchParameter", searchParameter);

                // number of total rows
                countAll = mapper.QueryForObject<Int32>(typeof(T).Name + ".CountBySearchParameter", searchParameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnList;
        }
        #endregion

        #region Converters
        public String ConvertParamFromString(String param)
        {
            return String.IsNullOrEmpty(param) ? "null" : "'" + param + "'";
        }

        public String ConvertParamFromInt32(Int32 param)
        {
            return param == 0 ? "null" : "'" + param.ToString() + "'";
        }
        
        #endregion
    }
}