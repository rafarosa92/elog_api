﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.TrackingLibSqlServer.Business
{
    public class NvoccBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion


        public IList<Nvocc> ProcBuscaLotes(Nvocc nvocc)
        {
            _log.Info("Begin Method");

            IList<Nvocc> returnValue = null;

            try
            {
                NvoccDAO nvoccDAO = new NvoccDAO();
                returnValue = nvoccDAO.ProcGetNvocc(nvocc);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Nvocc> ProcBloqDesbloq(Nvocc nvocc)
        {
            _log.Info("Begin Method");

            IList<Nvocc> returnValue = null;

            try
            {
                NvoccDAO nvoccDAO = new NvoccDAO();
                returnValue = nvoccDAO.ProcBloqDesbloq(nvocc);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

    }
}