﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.TrackingLibSqlServer.Business
{
    public class AverbacaoUnidadeBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public AverbacaoUnidade GetPathFile(Int32 Uni_ID)
        {
            _log.Info("Begin Method");

            AverbacaoUnidade returnValue = null;

            try
            {
                AverbacaoUnidadeDAO averbacaoUnidadeDAO = new AverbacaoUnidadeDAO();
                returnValue = averbacaoUnidadeDAO.SelectPathFile(Uni_ID);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public AverbacaoUnidade GetById(Int32 Uni_ID)
        {
            _log.Info("Begin Method");

            AverbacaoUnidade returnValue = null;

            try
            {
                AverbacaoUnidadeDAO averbacaoUnidadeDAO = new AverbacaoUnidadeDAO();
                returnValue = averbacaoUnidadeDAO.SelectPathFile(Uni_ID);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<AverbacaoUnidade> GetParamComprovante(Int32 Uni_ID)
        {
            _log.Info("Begin Method");

            IList<AverbacaoUnidade> returnValue = null;

            try
            {
                AverbacaoUnidadeDAO averbacaoUnidadeDAO = new AverbacaoUnidadeDAO();
                returnValue = averbacaoUnidadeDAO.SelectParamComprovante(Uni_ID);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

    }
}