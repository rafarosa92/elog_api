﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;
using MBLabs.TrackingLibSqlServer.Mechanism;
using System;
using System.IO;
using System.Collections.Generic;
using Ionic.Zip;

namespace MBLabs.TrackingLibSqlServer.Business
{
    public class LocalizacaoBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
        public IList<Localizacao> GetDadosLocalizacao(Localizacao localizacao)
        {
            _log.Info("Begin Method");

            IList<Localizacao> returnValue = null;

            try
            {
                LocalizacaoDAO localizacaoDAO = new LocalizacaoDAO();
                returnValue = localizacaoDAO.SelectLocalizacaoByLoteID(localizacao);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}
