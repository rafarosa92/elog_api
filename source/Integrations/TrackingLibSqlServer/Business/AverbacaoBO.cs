﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;

using System;
using System.Collections.Generic;
using System.IO;

namespace MBLabs.TrackingLibSqlServer.Business
{
    public class AverbacaoBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public Averbacao SaveDI(Averbacao averbacao)
        {
            _log.Info("Begin Method");

            Averbacao returnValue = null;

            try
            {
                AverbacaoDAO averbacaoDAO = new AverbacaoDAO();
                returnValue = averbacaoDAO.InsertAverbacao(averbacao);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Averbacao> GetAverbacaoByDate(SelectByDate selectByDate)
        {
            _log.Info("Begin Method");

            IList<Averbacao> returnValue = null;

            try
            {
                AverbacaoDAO averbacaoDAO = new AverbacaoDAO();
                returnValue = averbacaoDAO.SelectAverbacaoByDate(selectByDate);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Averbacao> GetDadosDI(Averbacao averbacao)
        {
            _log.Info("Begin Method");

            IList<Averbacao> returnValue = null;

            try
            {
                AverbacaoDAO averbacaoDAO = new AverbacaoDAO();
                returnValue = averbacaoDAO.SelectDadosDI(averbacao);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public Averbacao AlteraResponsavel(Averbacao averbacao)
        {
            _log.Info("Begin Method");

            Averbacao returnValue = null;

            try
            {
                AverbacaoDAO averbacaoDAO = new AverbacaoDAO();
                returnValue = averbacaoDAO.AlteraResponsavel(averbacao);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public Averbacao CancelarDI(Averbacao averbacao)
        {
            _log.Info("Begin Method");

            Averbacao returnValue = null;

            try
            {
                AverbacaoDAO averbacaoDAO = new AverbacaoDAO();
                returnValue = averbacaoDAO.CancelarDI(averbacao);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public Averbacao ReprocessarDI(Averbacao averbacao)
        {
            _log.Info("Begin Method");

            Averbacao returnValue = null;

            try
            {
                AverbacaoDAO averbacaoDAO = new AverbacaoDAO();
                returnValue = averbacaoDAO.ReprocessarDI(averbacao);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}