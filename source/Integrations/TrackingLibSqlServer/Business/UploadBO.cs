﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;
using MBLabs.TrackingLibSqlServer.Mechanism;
using System;
using System.IO;
using System.Collections.Generic;
using Ionic.Zip;

namespace MBLabs.TrackingLibSqlServer.Business
{
    public class UploadBO   
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
        public Upload SaveFile(Upload upload)
        {
            _log.Info("Begin Method");

            Upload returnValue = null;

            try
            {
                UploadDAO uploadDAO = new UploadDAO();
                upload.NameFile = uploadDAO.getFileName(upload).NameFile;
                uploadDAO.InsertSaveFile(upload);

                ZipFile zip = new ZipFile();
                Byte[] file = Convert.FromBase64String(upload.HashFile);
                MemoryStream memoryStream = new MemoryStream(file);
                zip.AddEntry(upload.NameFile + upload.ExtensionFile, memoryStream);

                using (new Impersonator("ELOGCORP", "s_tracking", "123@elog"))
                {
                    zip.Save(@""+upload.PathFile+"\\" + upload.NameFile + ".zip");
                }

                    
                returnValue = upload;

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public Upload DeleteFile(Upload upload)
        {
            _log.Info("Begin Method");

           Upload returnValue = null;

            try
            {
                UploadDAO uploadDAO = new UploadDAO();
                uploadDAO.DeleteFile(upload);

                using (new Impersonator("ELOGCORP", "s_tracking", "123@elog"))
                {
                    File.Delete(@""+upload.PathFile + "\\" + upload.NameFile);
                    upload.Action = "Delete File Success";
                    returnValue = upload;
                }


                
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Upload> GetLogDocByDiNumero(String DiNumero)
        {
            _log.Info("Begin Method");

            IList<Upload> returnValue = null;

            try
            {
                UploadDAO uploadDAO = new UploadDAO();
                returnValue = uploadDAO.SelectLogDocByDiNumero(DiNumero);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Upload> GetLogDocByDI_ID(Int32 DI_ID)
        {
            _log.Info("Begin Method");

            IList<Upload> returnValue = null;

            try
            {
                UploadDAO uploadDAO = new UploadDAO();
                returnValue = uploadDAO.SelectLogDocByDI_ID(DI_ID);

            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public IList<Upload> DownloadHash(Upload upload)
        {
            _log.Info("Begin Method");

            IList<Upload> returnValue = new List<Upload>();
            //IList<Upload> Arquivos = new List<Upload>();
            try
            {

                UploadDAO uploadDAO = new UploadDAO();


                Int32 tama = uploadDAO.SelectLogDocByDI_ID(upload.DI_ID).Count;
                for (int i = 0; i < tama; i++)
                {
                    upload.NameFile = i.ToString();
                    using (new Impersonator("ELOGCORP", "s_tracking", "123@elog"))
                    {

                        var nome_documento = uploadDAO.SelectLogDocByDI_ID(upload.DI_ID)[i].Ld_NomeDocumento;
                        if (nome_documento.IndexOf(".xml")>0 && nome_documento.IndexOf("NF")>0)
                        {
                            nome_documento = nome_documento.Insert(nome_documento.IndexOf("NF"), "a");
                        }


                        Byte[] bytes = File.ReadAllBytes(@"" + upload.PathFile + "\\" + nome_documento);
                        String encdoed = Convert.ToBase64String(bytes);
                        upload.HashFile =  encdoed;
                    }

                    returnValue.Add(new Upload()
                        {
                            NameFile = uploadDAO.SelectLogDocByDI_ID(upload.DI_ID)[i].Ld_NomeDocumento,
                            HashFile = upload.HashFile,
                            DateCreate = uploadDAO.SelectLogDocByDI_ID(upload.DI_ID)[i].DateCreate,
                            Ld_UCriacao = uploadDAO.SelectLogDocByDI_ID(upload.DI_ID)[i].Ld_UCriacao,

                    }
                    );
                }

                

                //returnValue = Arquivos;
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

    }
}
