﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;
using MBLabs.TrackingLibSqlServer.Mechanism;
using System;
using System.IO;
using System.Collections.Generic;
using Ionic.Zip;

namespace MBLabs.TrackingLibSqlServer.Business
{
    public class InfoUnidadeBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
        public IList<InfoUnidade> GetDadosUniInfo(InfoUnidade infoUnidade)
        {
            _log.Info("Begin Method");

            IList<InfoUnidade> returnValue = null;

            try
            {
                InfoUnidadeDAO infoUnidadeDAO = new InfoUnidadeDAO();
                returnValue = infoUnidadeDAO.SelectAll(infoUnidade);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}
