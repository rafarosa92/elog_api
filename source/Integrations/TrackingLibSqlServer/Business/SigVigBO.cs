﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;
using MBLabs.TrackingLibSqlServer.Mechanism;
using System;
using System.IO;
using System.Collections.Generic;
using Ionic.Zip;

namespace MBLabs.TrackingLibSqlServer.Business
{
    public class SigVigBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
        public IList<SigVig> GetBusca(SigVig sigVig)
        {
            _log.Info("Begin Method");

            IList<SigVig> returnValue = null;

            try
            {
                SigVigDAO sigVigDAO = new SigVigDAO();
                returnValue = sigVigDAO.SelectProcBusca(sigVig);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<SigVig> VerificaPosData(SigVig sigVig)
        {
            _log.Info("Begin Method");

            IList<SigVig> returnValue = null;

            try
            {
                SigVigDAO sigVigDAO = new SigVigDAO();
                returnValue = sigVigDAO.SelectPosData(sigVig);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<SigVig> VerificaExcecao(SigVig sigVig)
        {
            _log.Info("Begin Method");

            IList<SigVig> returnValue = null;

            try
            {
                SigVigDAO sigVigDAO = new SigVigDAO();
                returnValue = sigVigDAO.SelectExcecao(sigVig);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<SigVig> SelectQtdPadrao(SigVig sigVig)
        {
            _log.Info("Begin Method");

            IList<SigVig> returnValue = null;

            try
            {
                SigVigDAO sigVigDAO = new SigVigDAO();
                returnValue = sigVigDAO.SelectQtdPadrao(sigVig);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<SigVig> SelectQtdMaxDia(SigVig sigVig)
        {
            _log.Info("Begin Method");

            IList<SigVig> returnValue = null;

            try
            {
                SigVigDAO sigVigDAO = new SigVigDAO();
                returnValue = sigVigDAO.SelectQtdMaxDia(sigVig);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<SigVig> ProcAgenda(SigVig sigVig)
        {
            _log.Info("Begin Method");

            IList<SigVig> returnValue = null;

            try
            {
                SigVigDAO sigVigDAO = new SigVigDAO();
                returnValue = sigVigDAO.ProcAgenda(sigVig);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<SigVig> ProcMapaSara(SigVig sigVig)
        {
            _log.Info("Begin Method");

            IList<SigVig> returnValue = null;

            try
            {
                SigVigDAO sigVigDAO = new SigVigDAO();
                returnValue = sigVigDAO.ProcMapaSara(sigVig);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}


