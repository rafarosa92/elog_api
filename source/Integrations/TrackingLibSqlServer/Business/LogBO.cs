﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;
using MBLabs.TrackingLibSqlServer.Mechanism;
using System;
using System.IO;
using System.Collections.Generic;
using Ionic.Zip;

namespace MBLabs.TrackingLibSqlServer.Business
{
    public class LogBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
        public Log SaveLog(Log log)
        {
            _log.Info("Begin Method");

            Log returnValue = null;

            try
            {
                LogDAO logDAO = new LogDAO();
                returnValue = logDAO.InsertLog(log); 
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public IList<Log> GetLogByID(Log log)
        {
            _log.Info("Begin Method");

            IList<Log> returnValue = null;

            try
            {
                LogDAO logDAO = new LogDAO();
                returnValue = logDAO.GetLogByID(log);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public IList<Log> GetPendencia(Log log)
        {
            _log.Info("Begin Method");

            IList<Log> returnValue = null;

            try
            {
                LogDAO logDAO = new LogDAO();
                returnValue = logDAO.ProcPendencia(log);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}
