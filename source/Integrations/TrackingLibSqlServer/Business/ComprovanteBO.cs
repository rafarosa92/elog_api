﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;
using MBLabs.TrackingLibSqlServer.Mechanism;
using System;
using System.IO;
using System.Collections.Generic;
using Ionic.Zip;

namespace MBLabs.TrackingLibSqlServer.Business
{
    public class ComprovanteBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
        public IList<Comprovante> GetDadosLote(Comprovante comprovante)
        {
            _log.Info("Begin Method");

            IList<Comprovante> returnValue = null;

            try
            {
                ComprovanteDAO comprovanteDAO = new ComprovanteDAO();
                returnValue = comprovanteDAO.SelectDadosLote(comprovante);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Comprovante> GetPesoAferido(Comprovante comprovante)
        {
            _log.Info("Begin Method");

            IList<Comprovante> returnValue = null;

            try
            {
                ComprovanteDAO comprovanteDAO = new ComprovanteDAO();
                returnValue = comprovanteDAO.ProcPesoAferido(comprovante);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Comprovante> GetEspecie(Comprovante comprovante)
        {
            _log.Info("Begin Method");

            IList<Comprovante> returnValue = null;

            try
            {
                ComprovanteDAO comprovanteDAO = new ComprovanteDAO();
                returnValue = comprovanteDAO.GetEspecie(comprovante);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Comprovante> GetAvaria(Comprovante comprovante)
        {
            _log.Info("Begin Method");

            IList<Comprovante> returnValue = null;

            try
            {
                ComprovanteDAO comprovanteDAO = new ComprovanteDAO();
                returnValue = comprovanteDAO.GetAvaria(comprovante);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}
