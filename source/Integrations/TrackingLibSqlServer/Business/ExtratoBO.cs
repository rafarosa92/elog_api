﻿using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Persistence.DAO;
using MBLabs.TrackingLibSqlServer.Transfer;
using MBLabs.TrackingLibSqlServer.Mechanism;
using System;
using System.IO;
using System.Collections.Generic;
using Ionic.Zip;

namespace MBLabs.TrackingLibSqlServer.Business
{
    public class ExtratoBO
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
        public IList<Extrato> GetDadosLote(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosLote(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetDadosCESVEntrada(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosCESVEntrada(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetDadosDocEntrada(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosDocEntrada(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetDadosVolRecebidos(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosVolRecebidos(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetDadosVolRecebidosPendentes(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosVolRecebidosPendentes(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetDadosAvariaLote(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosAvariaLote(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetDadosConteiner(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosConteiner(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetAvariaConteiner(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectAvariaConteiner(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetLacreConteiner(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectLacreConteiner(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetProcPreFiltro(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.ProcExtratoPreFiltro(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetDadosDocSaida(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosDocSaida(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public IList<Extrato> GetDadosCESVSaida(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosCESVSaida(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetDadosFaturamento(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosFaturamento(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetDadosDaCda(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosDaCda(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetDadosApreensao(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.SelectDadosApreensao(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public IList<Extrato> GetProcPeso(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.ProcPeso(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Extrato> GetProcPesoAferido(Extrato extrato)
        {
            _log.Info("Begin Method");

            IList<Extrato> returnValue = null;

            try
            {
                ExtratoDAO extratoDAO = new ExtratoDAO();
                returnValue = extratoDAO.ProcPesoAferido(extrato);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

    }
}


