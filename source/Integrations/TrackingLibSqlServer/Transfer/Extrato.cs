﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class Extrato
    {
        public String lote_id { get; set; }
        public String lote_conhec { get; set; }
        public String modal { get; set; }
        public String reg_nome { get; set; }
        public DateTime lote_dt_conhec { get; set; }
        public String cli_nome { get; set; }
        public String cli_cgc { get; set; }
        public String lote_pcarga { get; set; }
        public String lote_manifesto { get; set; }
        public String proc_nome { get; set; }
        public DateTime lote_dt_venc { get; set; }
        public Decimal lote_cif_tot_us { get; set; }
        public Decimal lote_cif_tot_br { get; set; }
        public Decimal lote_peso_bruto { get; set; }
        public Decimal lote_qt_tot { get; set; }
        public String pais_nome { get; set; }
        public String exportador { get; set; }
        public String cp_descricao { get; set; }
        public String descricao { get; set; }
        public String sobre_rodas { get; set; }
        public String CompanyName { get; set; }
        public String cesv_id { get; set; }
        public String vei_id { get; set; }
        public String vei_id_reb { get; set; }
        public DateTime cesv_dt_entrada { get; set; }
        public DateTime cesv_dt_saida { get; set; }
        public String cnt_id { get; set; }
        public String dent_id { get; set; }
        public String doc_id { get; set; }
        public String numero { get; set; }
        public String serie { get; set; }
        public DateTime dent_dt_emissao { get; set; }
        public Decimal oitem_qt { get; set; }
        public String esp_id { get; set; }
        public Decimal oitem_m3 { get; set; }
        public String ter_id { get; set; }
        public Int32 tas_qtd { get; set; }
        public String ava_descricao { get; set; }
        public String reg_imp_exp { get; set; }
        public String presenca { get; set; }
        public Decimal peso_aferido { get; set; }


        public String lote_cancelado { get; set; }
        public String nav_n2 { get; set; }
        public String porto_nome { get; set; }
        public String lote_tipo { get; set; }
        public String consolidador { get; set; }

        public String cnt_tipo { get; set; }
        public Decimal cnt_tara { get; set; }
        public String tiso_descricao { get; set; }
        public DateTime mcnt_dt_desova { get; set; }
        public DateTime mcnt_dt_ova { get; set; }
        public Decimal manifestado { get; set; }
        public Decimal peso_entrada { get; set; }
        public Decimal peso_saida { get; set; }
        public Decimal aferido { get; set; }
        public Decimal diferenca_mesmo_vei { get; set; }
        public Decimal porc_dif_mesmo_vei { get; set; }
        public Decimal diferenca_baixa { get; set; }
        public Decimal porc_dif_baixa { get; set; }
        public String saiu_mesmo_vei { get; set; }
        public String tmc_descricao { get; set; }
        public String recebimento { get; set; }
       
        public String pcnt_descricao { get; set; }

        public String lac_numero { get; set; }
        public String lac_descricao { get; set; }

        public Int32 usu_id { get; set; }
        public Int32 uni_id { get; set; }
        public String ComissariaCNPJ { get; set; }
        public String BeneficiarioCNPJ { get; set; }
        public String TPU_VALOR { get; set; }

        public String CNPJs { get; set; }
        public String Tipo { get; set; }

        
        public String dsai_id { get; set; }
        public DateTime dsai_dt_registro { get; set; }
        public DateTime dsai_dt_desemb { get; set; }


        public Int32 nf_id { get; set; }
        public Decimal nfse_numero { get; set; }
        public DateTime nf_dt_emissao { get; set; }
        public Decimal nf_vlr_tot { get; set; }


        public String beneficiario { get; set; }
        public String doc_beneficiario { get; set; }
        public String comissaria { get; set; }
        public String doc_comissaria { get; set; }
        public Decimal dsai_cif_us { get; set; }
        public Decimal cif_br { get; set; }
        public DateTime data_cotacao { get; set; }
        public Decimal cot_valor1 { get; set; }
        public String canal { get; set; }
        public Int32 qtd_itens { get; set; }

        
        public String apr_nr_doc { get; set; }
        public DateTime apr_data { get; set; }
        public String ass_nome  { get; set; }
        public Decimal volumes { get; set; }

        public Decimal documental { get; set; }
        public Decimal diferenca { get; set; }
        public Decimal porcentagem { get; set; }
        public Decimal limite { get; set; }
        public Int32 ultrapassou_limite { get; set; }
        public String pesagem { get; set; }

        public Boolean acesso60 { get; set; }

    }
}