﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class Comprovante    
    {
        public Int32 Volumes { get; set; }
        public DateTime Lote_dt_venc { get; set; }
        public DateTime Entrada { get; set; }
        public String Familia { get; set; }
        public Decimal Cif { get; set;  }
        public String Lote_conhec { get; set; }
        public String Beneficiario { get; set; }
        public String Cli_doc { get; set; }
        public String Procedencia { get; set; }
        public String Onu { get; set; }
        public String Doc_entrada { get; set; }
        public String Tfa { get; set; }
        public String CompanyName { get; set; }
        public String DI_SLOTE_ID { get; set; }
        public String esp_id { get; set; }
        public String tipo { get; set; }
        public String TFA { get; set; }
        public Decimal peso_aferido { get; set; }


    }
}