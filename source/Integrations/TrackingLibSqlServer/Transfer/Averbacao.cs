﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class Averbacao
    {
        public Int32 DI_ID { get; set; }
        public String DI_Situacao { get; set; }
        public DateTime DI_DtCriacao { get; set; }
        public String DI_UCriacao { get; set; }
        public String DI_Numero { get; set; }
        public String DI_QCD_TIPO_DECLARACAO { get; set; }
        public String DI_QDT_REGISTRO_DI { get; set; }
        public String DI_QDT_DESEMBARACO { get; set; }
        public String DI_QVL_TOTAL_MLE_DOLAR { get; set; }
        public String DI_QPB_CARGA { get; set; }
        public String DI_QPL_CARGA { get; set; }
        public String DI_QQUANTIDADE_DECLARADA { get; set; }
        public String DI_QQT_ADICAO_DI { get; set; }
        public String DI_QMODAL { get; set; }
        public String Di_QNR_CPF_RESPONS_REG { get; set; }
        public String DI_QNR_IMPORTADOR { get; set; }
        public String DI_QCD_MOEDA_FRETE { get; set; }
        public String DI_QCD_MOEDA_SEGURO { get; set; }
        public String DI_QNR_DCTO_CARGA { get; set; }
        public String DI_QNM_TIPO_DECLARACAO { get; set; }
        public Int32 Tsa_ID { get; set; }
        public Int32 Uni_ID { get; set; }
        public String DI_CNPJ { get; set; }
        public DateTime DI_DtGeracaoArq { get; set; }
        public String DI_Erro { get; set; }
        public DateTime DI_DtValidacaoQuorum { get; set; }
        public String DI_DescricaoErro { get; set; }
        public String DI_QNM_URF_ENTR_CARGA { get; set; }
        public DateTime DI_DtValidacaoSara { get; set; }
        public String DI_QVL_TOTAL_II_A_REC { get; set; }
        public String DI_QVL_TOTAL_IPI_A_REC { get; set; }
        public String DI_QVL_TOTAL_II_SUSP { get; set; }
        public String DI_QVL_TOTAL_IPI_SUSP { get; set; }
        public String DI_QVL_FRETE_TNAC_MNEG { get; set; }
        public String DI_QVL_TOT_SEGURO_MNEG { get; set; }
        public String DI_SDSAI_ID { get; set; }
        public String DI_SCLI_ID { get; set; }
        public String DI_SCOM_ID { get; set; }
        public String DI_SREP_ID { get; set; }
        public String DI_STFL_ID { get; set; }
        public String DI_SMOEDA { get; set; }
        public String DI_SCOTACAO { get; set; }
        public String DI_SDOLAR { get; set; }
        public String DI_SMOEDASEG { get; set; }
        public String DI_SCOTACAOSEG { get; set; }
        public String DI_SDOLARSEG { get; set; }
        public String DI_SLOTE_ID { get; set; }
        public DateTime DI_DtEnvioEmail { get; set; }
        public String DI_EnviaEmail { get; set; }
        public String DI_LiberadoInclusaoSara { get; set; }
        public DateTime DI_DtInclusaoSara { get; set; }
        public String DI_SSIS_AVERBACAO { get; set; }
        public String DI_QCD_CANAL { get; set; }
        public String DI_QNR_DCTO_INSTRUCAO { get; set; }
        public String DI_QVL_TOTAL_MLD_DOLAR { get; set; }
        public String DI_SCOTACAO_DIA { get; set; }
        public String DI_SCONSOLIDADOR { get; set; }
        public String DI_QNM_IMPORTADOR { get; set; }
        public String DI_NomeComissaria { get; set; }
        public String DI_SREP_NOME { get; set; }
        public String DI_ImpostoII { get; set; }
        public String DI_ImpostoIPI { get; set; }
        public String DI_ImpostoPIS { get; set; }
        public String DI_ImpostoCOFINS { get; set; }
        public String DI_ImpostoDIREITOS { get; set; }
        public String DI_ImpostoIPITipo { get; set; }
        public String DI_ImpostoIITipo { get; set; }
        public String DI_ImpostoPISTipo { get; set; }
        public String DI_ImpostoCOFINSTipo { get; set; }
        public String DI_ImpostoDIREITOSTipo { get; set; }
        public String DI_Referencia { get; set; }
        public String DI_QNR_DCTO_CARGA_MAST { get; set; }
        public String DI_ImpostoVemQuorum { get; set; }
        public String Di_Responsavel { get; set; }
        public String Di_Observacao { get; set; }
        public DateTime DI_DtLiberacao { get; set; }
        public String DI_QNR_CPF_REPR_LEGAL { get; set; }
        public String DI_QNM_REPR_LEGAL { get; set; }
        public String DI_E { get; set; }
        public String CompanyName { get; set; }
        public String u_descricao { get; set; }
        public String tsa_descricao { get; set; }
        public String DI_EA { get; set; }



    }
}