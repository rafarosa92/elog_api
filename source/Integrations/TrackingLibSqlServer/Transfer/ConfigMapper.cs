﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public static class ConfigMapper
    {
        public static String SqlMapSQLSERVER_Tracking = "MBLabs.TrackingLibSqlServer.Configuration.IBatis.SqlMapSQLSERVER_Tracking.config";
        public static String SqlMapSQLSERVER_Tracking_HOM = "MBLabs.TrackingLibSqlServer.Configuration.IBatis.SqlMapSQLSERVER_Tracking_HOM.config";
        public static String SqlMapSQLSERVER_SaraBarueri = "MBLabs.TrackingLibSqlServer.Configuration.IBatis.SqlMapSQLSERVER_SaraBarueri.config";
        public static String SqlMapSQLSERVER_SaraSantos = "MBLabs.TrackingLibSqlServer.Configuration.IBatis.SqlMapSQLSERVER_SaraSantos.config";
        public static String SqlMapSQLSERVER_SaraSantos_HOM = "MBLabs.TrackingLibSqlServer.Configuration.IBatis.SqlMapSQLSERVER_SaraSantos_HOM.config";
        public static String SqlMapSQLSERVER_SaraCampinas = "MBLabs.TrackingLibSqlServer.Configuration.IBatis.SqlMapSQLSERVER_SaraCampinas.config";
        public static String SqlMapSQLSERVER_SaraSP = "MBLabs.TrackingLibSqlServer.Configuration.IBatis.SqlMapSQLSERVER_SaraSP.config";
        public static String SqlMapMYSQL = "MBLabs.TrackingLibSqlServer.Configuration.IBatis.SqlMapMYSQL .config";

        public static String BancoUsado = String.Empty;

    }
}
