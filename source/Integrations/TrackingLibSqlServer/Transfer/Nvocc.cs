﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class Nvocc
    {
        public String lote_master { get; set; }
        public String lote_master_conhec { get; set; }
        public String lote_id { get; set; }
        public String cnt_id { get; set; }
        public String lote_tipo { get; set; }
        public String lote_conhec { get; set; }
        public String cli_nome { get; set; }
        public Int32 lote_bloqueado_nvocc { get; set; }
        public Int32 usu_id { get; set; }
        public String usu_desbloq { get; set; }
        public String usu_data_desbloq { get; set; }
        public String saldo { get; set; }
        public String subnvocc { get; set; }
        public String cli_cnpj { get; set; }
        public String CompanyName { get; set; }
        public String usuario { get; set; }
        public String acao { get; set; }


    }
}