﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class AverbacaoUnidade
    {
        public String Uni_CaminhoArq { get; set; }
        public String Uni_InformaCanal { get; set; }
        public String CompanyName { get; set; }
        public String Uni_InformaReferencia { get; set; }
        public String uni_ativarvalidacaovalor { get; set; }
        public String uni_ativarvalidacaoqtde { get; set; }
        public String uni_ativarvalidacaopeso { get; set; }
        public Int32 Uni_ID { get; set; }

    }
}