﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class Seizure
    {
        public Int32 doc_id { get; set; }
        public String Login { get; set; }
    }
}