﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class SigVig
    {
        
        public String CompanyName { get; set; }

        public String cnt_id { get; set; }
        public String lote_conhec { get; set; }
        public String lote_tipo { get; set; }
        public String lote_manifesto { get; set; }
        public String litem_descricao { get; set; }
        public String sit_unidade { get; set; }
        public String sit_manifesto { get; set; }
        public DateTime data_status { get; set; }


        public Int32 SAG_ID { get; set; }
        public Int32 USU_ID { get; set; }
        public DateTime SAG_DATA     { get; set; }
        public String SAG_CPF1 { get; set; }
        public String SAG_CPF2 { get; set; }
        public String SAG_CPF3 { get; set; }
        public DateTime SAG_DATA_AG { get; set; }
        public String SAG_STATUS { get; set; }

        public Int32 svg_id { get; set; }
        public DateTime svg_data { get; set; }
        public Int32 svg_qtde { get; set; }
        public DateTime svg_cadastro { get; set; }

    }
}