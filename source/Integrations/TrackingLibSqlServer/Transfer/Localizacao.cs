﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class Localizacao
    {
        public String arm_id { get; set; }
        public String cel_quadra { get; set; }
        public String cel_rua { get; set; }
        public String cel_bloco { get; set; }
        public String cel_nivel { get; set; }
        public String cel_posicao { get; set; }
        public String DI_SLOTE_ID { get; set; }
        public String CompanyName { get; set; }




    }
}