﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class Log
    {
        public Int32 Le_ID { get; set; }
        public DateTime Le_DtCriacao { get; set; }
        public String Le_UCriacao { get; set; }
        public Int32 DI_ID { get; set; }
        public String Le_Descricao { get; set; }
        public String pendencia { get; set; }
        public String UniPendencia { get; set; }



    }

    public class LogMsg
    {
       public string MsgCancelamento = "Cancelamento executado pelo usuário";
       public string MsgResponsavel = "Alterado o responsável pela DI, antes era: ";
       public string MsgReprocessamento = "Reprocessamento executado pelo usuário. Data de criação anterior do registro foi em: ";
       public string MsgRevisao = "Gerada solicitação de revisão de processos de liberação";
    }
}