﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class SelectByDate
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public Int32 Uni_ID { get; set; }
        public String CompanyName { get; set; }
        public String DI_UCriacao { get; set; }



    }
}
