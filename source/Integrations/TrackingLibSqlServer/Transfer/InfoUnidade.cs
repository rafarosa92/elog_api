﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class InfoUnidade
    {
        public String sis_endereco { get; set; }
        public String sis_bairro { get; set; }
        public String sis_cidade { get; set; }
        public String sis_cep { get; set; }
        public String sis_uf { get; set; }
        public String sis_telefone { get; set; }
        public String CompanyName { get; set; }


    }
}