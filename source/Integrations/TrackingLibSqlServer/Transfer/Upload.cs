﻿using System;

namespace MBLabs.TrackingLibSqlServer.Transfer
{
    public class Upload
    {
        public String NameFile { get; set; }
        public String HashFile { get; set; }
        public String Ld_UCriacao { get; set; }
        public String Action { get; set; }
        public String Ld_NomeDocumento { get; set; }
        public DateTime DateCreate { get; set; }
        public Int32 DI_ID { get; set; }
        public Int32 Id_LogDoc { get; set; }
        public Int32 Uni_ID { get; set; }
        public String TypeFile { get; set; }
        public String ExtensionFile { get; set; }
        public String PathFile { get; set; }
        public String CompanyName { get; set; }
        public String DI_Numero { get; set; }


    }
}