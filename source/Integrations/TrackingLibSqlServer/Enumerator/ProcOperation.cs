﻿namespace MBLabs.TrackingLibSqlServer.Enumerator
{
    public enum ProcOperation
    {
        S,
        L,
        B,
        I,
        U
    }
}