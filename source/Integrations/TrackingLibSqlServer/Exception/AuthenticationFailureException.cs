﻿using System;

namespace MBLabs.TrackingLibSqlServer.Exception
{
    [Serializable]
    public class AuthenticationFailureException : System.Exception
    {
        public AuthenticationFailureException(System.Exception innerExc)
            : base(innerExc.GetType().Name, innerExc)
        {
        }

        public AuthenticationFailureException(String innerExc)
            : base(innerExc)
        {
        }
    }
}