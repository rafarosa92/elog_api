﻿using System;

namespace MBLabs.TrackingLibSqlServer.Exception
{
    [Serializable]
    public class PersistenceException : ApplicationException
    {
        public PersistenceException(System.Exception innerExc) : base(innerExc.GetType().Name, innerExc)
        {
        }
    }
}