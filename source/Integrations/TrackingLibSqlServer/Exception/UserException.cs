﻿using System;

namespace MBLabs.TrackingLibSqlServer.Exception
{
    [Serializable]
    public class UserException : System.Exception
    {
        public UserException(System.Exception innerExc)
            : base(innerExc.GetType().Name, innerExc)
        {
        }

        public UserException(String innerExc)
            : base(innerExc)
        {
        }
    }
}