﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace MBLabs.TrackingLibSqlServer.Util
{
    public class XMLUtil
    {
        public static String ToXML(Object obj)
        {
            using (StringWriter stringwriter = new Utf8StringWriter())
            {
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(stringwriter, obj);

                return stringwriter.ToString();
            }
        }

        /*
        public static Object LoadFromXMLString(string xmlText, Type type)
        {
            var stringReader = new System.IO.StringReader(xmlText);
            var serializer = new XmlSerializer(type);

            return serializer.Deserialize(stringReader) as type;
        }*/
    }

    public class Utf8StringWriter : StringWriter
    {
        // Use UTF8 encoding but write no BOM to the wire
        public override Encoding Encoding
        {
            get { return new UTF8Encoding(false); } // in real code I'll cache this encoding.
        }
    }
}