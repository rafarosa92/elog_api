﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections;
using System.Collections.Generic;
using MBLabs.TrackingLibSqlServer;


namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    public class NvoccDAO  : DAOManager<Nvocc>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public IList<Nvocc> ProcGetNvocc(Nvocc nvocc)
        {
            _log.Info("Begin Method");
            IList<Nvocc> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(nvocc.CompanyName));


                IDictionary<String, String> parameter = new Dictionary<String, String>();
                parameter["cli_cnpj"] = base.ConvertParamFromString(nvocc.cli_cnpj);
                parameter["lote_master_conhec"] = base.ConvertParamFromString(nvocc.lote_master_conhec);
                parameter["cnt_id"] = base.ConvertParamFromString(nvocc.cnt_id);

                returnValue = mapper.QueryForList<Nvocc>("Nvocc.ProcBuscaLote", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public IList<Nvocc> ProcBloqDesbloq(Nvocc nvocc)
        {
            _log.Info("Begin Method");
            IList<Nvocc> returnValue = null;

            try
            {
                
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(nvocc.CompanyName));


                IDictionary<String, String> parameter = new Dictionary<String, String>();
                parameter["lote_id"] = base.ConvertParamFromString(nvocc.lote_id);
                parameter["usuario"] = base.ConvertParamFromString(nvocc.usuario);
                parameter["acao"] = base.ConvertParamFromString(nvocc.acao);

                returnValue = mapper.QueryForList<Nvocc>("Nvocc.ProcBloqDesbloq", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        #endregion
    }
}