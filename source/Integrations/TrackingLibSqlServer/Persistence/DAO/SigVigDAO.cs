﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    public class SigVigDAO : DAOManager<SigVig>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
       
        public IList<SigVig> SelectProcBusca(SigVig sigVig)
        {
            _log.Info("Begin Method");
            IList<SigVig> returnValue = null;
            
            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["lote_conhec"] = base.ConvertParamFromString(sigVig.lote_conhec);
            parameter["cnt_id"] = base.ConvertParamFromString(sigVig.cnt_id);
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(sigVig.CompanyName));
                returnValue = mapper.QueryForList<SigVig>("SigVig.ProcBusca",parameter);
                

            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }

        public IList<SigVig> SelectPosData(SigVig sigVig)
        {
            _log.Info("Begin Method" + sigVig.SAG_DATA.ToString("yyyy-MM-dd"));
            IList<SigVig> returnValue = null;

            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["SAG_DATA"] = base.ConvertParamFromString(sigVig.SAG_DATA.ToString("yyyy-MM-dd"));
            parameter["cnt_id"] = base.ConvertParamFromString(sigVig.cnt_id);
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapMYSQL);
                returnValue = mapper.QueryForList<SigVig>("SigVig.SelectPosData", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<SigVig> SelectExcecao(SigVig sigVig)
        {
            _log.Info("Begin Method" + sigVig.SAG_DATA.ToString("yyyy-MM-dd"));
            IList<SigVig> returnValue = null;

            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["SAG_DATA"] = base.ConvertParamFromString(sigVig.SAG_DATA.ToString("yyyy-MM-dd"));
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapMYSQL);
                returnValue = mapper.QueryForList<SigVig>("SigVig.SelectExcecao", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<SigVig> SelectQtdPadrao(SigVig sigVig)
        {
            _log.Info("Begin Method" + sigVig.SAG_DATA.ToString("yyyy-MM-dd"));
            IList<SigVig> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapMYSQL);
                returnValue = mapper.QueryForList<SigVig>("SigVig.SelectQtdPadrao", null);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<SigVig> SelectQtdMaxDia(SigVig sigVig)
        {
            _log.Info("Begin Method " + sigVig.SAG_DATA.ToString("yyyy-MM-dd"));
            IList<SigVig> returnValue = null;

            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["SAG_DATA"] = base.ConvertParamFromString(sigVig.SAG_DATA.ToString("yyyy-MM-dd"));
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapMYSQL);
                returnValue = mapper.QueryForList<SigVig>("SigVig.SelectQtdMaxDia", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<SigVig> ProcAgenda(SigVig sigVig)
        {
            _log.Info("Begin Method " + sigVig.SAG_DATA.ToString("yyyy-MM-dd"));
            IList<SigVig> returnValue = null;

            //$USU_ID$, $SAG_DATA$, $cnt_id$, $CPF1$, $CPF2$, $CPF3$

            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["USU_ID"] = base.ConvertParamFromInt32(sigVig.USU_ID);
            parameter["SAG_DATA"] = base.ConvertParamFromString(sigVig.SAG_DATA.ToString("yyyy-MM-dd"));
            parameter["cnt_id"] = base.ConvertParamFromString(sigVig.cnt_id);
            parameter["SAG_CPF1"] = base.ConvertParamFromString(sigVig.SAG_CPF1);
            parameter["SAG_CPF2"] = base.ConvertParamFromString(sigVig.SAG_CPF2);
            parameter["SAG_CPF3"] = base.ConvertParamFromString(sigVig.SAG_CPF3);

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapMYSQL);
                returnValue = mapper.QueryForList<SigVig>("SigVig.ProcAgenda", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<SigVig> ProcMapaSara(SigVig sigVig)
        {
            _log.Info("Begin Method " + sigVig.SAG_DATA.ToString("dd/MM/yyyy"));
            IList<SigVig> returnValue = null;

            //$USU_ID$, $SAG_DATA$, $cnt_id$, $CPF1$, $CPF2$, $CPF3$

            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["lote_manifesto"] = base.ConvertParamFromString(sigVig.lote_manifesto);
            parameter["SAG_DATA"] = base.ConvertParamFromString(sigVig.SAG_DATA.ToString("dd/MM/yyyy"));
            parameter["cnt_id"] = base.ConvertParamFromString(sigVig.cnt_id);
            parameter["SAG_CPF1"] = base.ConvertParamFromString(sigVig.SAG_CPF1);
            parameter["SAG_CPF2"] = base.ConvertParamFromString(sigVig.SAG_CPF2);
            parameter["SAG_CPF3"] = base.ConvertParamFromString(sigVig.SAG_CPF3);

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_SaraSantos_HOM);
                returnValue = mapper.QueryForList<SigVig>("SigVig.ProcMapaSara", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }


    }
}




