﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    public class UploadDAO : DAOManager<Upload>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
        public Upload getFileName(Upload upload)
        {
            _log.Info("Begin Method");
            Upload returnValue = null;
            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();
                parameter["DOC"] = "DI";
                parameter["DI_ID"] = upload.DI_ID.ToString();
                parameter["TIPO"] = upload.TypeFile;
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForObject<Upload>("Upload.ProcRetornaNome", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        
        public Upload InsertSaveFile(Upload upload)
        {
            _log.Info("Begin Method");
            Upload returnValue = null;
            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();
               
                    parameter["Ld_NomeDocumento"] = upload.NameFile + ".zip";
                parameter["Ld_UCriacao"] = upload.Ld_UCriacao;
                parameter["DI_ID"] = upload.DI_ID.ToString();
                _log.Info("dados do insert na table"+upload.NameFile+" , "+ upload.Ld_UCriacao+ " , " + upload.DI_ID);
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForObject<Upload>("Upload.Insert", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public Upload DeleteFile(Upload upload)
        {
            _log.Info("Begin Method");
            Upload returnValue = null;
            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();
                parameter["Ld_NomeDocumento"] = upload.NameFile;
                
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForObject<Upload>("Upload.Delete", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }

        public IList<Upload> SelectLogDocByDiNumero(String Id)
        {
            _log.Info("Begin Method");

            IList<Upload> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForList<Upload>("Upload.getByDiNumero", Id);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Upload> SelectLogDocByDI_ID(Int32 Id)
        {
            _log.Info("Begin Method");

            IList<Upload> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForList<Upload>("Upload.getByDI_ID", Id);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
    }
}
