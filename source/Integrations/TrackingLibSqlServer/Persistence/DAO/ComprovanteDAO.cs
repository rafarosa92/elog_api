﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    public class ComprovanteDAO : DAOManager<Comprovante>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
       
        public IList<Comprovante> SelectDadosLote(Comprovante comprovante)
        {
            _log.Info("Begin Method");
            IList<Comprovante> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(comprovante.CompanyName));
                returnValue = mapper.QueryForList<Comprovante>("Comprovante.SelectByLoteID", comprovante.DI_SLOTE_ID);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Comprovante> ProcPesoAferido(Comprovante comprovante)
        {
            _log.Info("Begin Method");
            IList<Comprovante> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(comprovante.CompanyName));
                returnValue = mapper.QueryForList<Comprovante>("Comprovante.ProcPeso", comprovante.DI_SLOTE_ID);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Comprovante> GetEspecie(Comprovante comprovante)
        {
            _log.Info("Begin Method");
            IList<Comprovante> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(comprovante.CompanyName));
                returnValue = mapper.QueryForList<Comprovante>("Comprovante.SelectEspecieByLoteID", comprovante.DI_SLOTE_ID);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Comprovante> GetAvaria(Comprovante comprovante)
        {
            _log.Info("Begin Method");
            IList<Comprovante> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(comprovante.CompanyName));
                returnValue = mapper.QueryForList<Comprovante>("Comprovante.SelectAvariaByTFA", comprovante.TFA);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
    }
}
