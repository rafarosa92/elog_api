﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    public class InfoUnidadeDAO : DAOManager<InfoUnidade>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
       
        public IList<InfoUnidade> SelectAll(InfoUnidade infoUnidade)
        {
            _log.Info("Begin Method");
            IList<InfoUnidade> returnValue = null;
            try
            {
                String ServerConnect = String.Empty;
                if (infoUnidade.CompanyName == "PS BARUERI")
                {
                    ServerConnect = ConfigMapper.SqlMapSQLSERVER_SaraBarueri;
                }
                if (infoUnidade.CompanyName == "CLIA CAMPINAS")
                {
                    ServerConnect = ConfigMapper.SqlMapSQLSERVER_SaraCampinas;
                }
                if (infoUnidade.CompanyName == "CLIA SANTOS")
                {
                    ServerConnect = ConfigMapper.SqlMapSQLSERVER_SaraSantos;
                }
                if (infoUnidade.CompanyName == "CLIA SAO PAULO")
                {
                    ServerConnect = ConfigMapper.SqlMapSQLSERVER_SaraSP;
                }
                ISqlMapper mapper = IBatisMapper.GetMapper(ServerConnect);
                returnValue = mapper.QueryForList<InfoUnidade>("InfoUnidade.SelectAll", null);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
    }
}
