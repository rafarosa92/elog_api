﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    public class ExtratoDAO : DAOManager<Extrato>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
       
        public IList<Extrato> SelectDadosLote(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));
                if (extrato.CompanyName == "CLIA SANTOS")
                {
                    returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosLoteSantos", base.ConvertParamFromString(extrato.lote_id));
                }
                else
                {
                    returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosLoteInterior", base.ConvertParamFromString(extrato.lote_id));
                }

            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }

        public IList<Extrato> SelectDadosCESVEntrada(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));
               
                    returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosCESVEntrada", base.ConvertParamFromString(extrato.lote_id));
                

            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> SelectDadosDocEntrada(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosDocEntrada", base.ConvertParamFromString(extrato.lote_id));


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }

        public IList<Extrato> SelectDadosVolRecebidos(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosVolRecebidos", base.ConvertParamFromString(extrato.lote_id));


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> SelectDadosVolRecebidosPendentes(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosVolRecebidosPendentes", base.ConvertParamFromString(extrato.lote_id));


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> SelectDadosAvariaLote(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosAvariaLote", base.ConvertParamFromString(extrato.lote_id));


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> SelectDadosConteiner(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosConteiner", base.ConvertParamFromString(extrato.lote_id));


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }

        public IList<Extrato> SelectAvariaConteiner(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["lote_id"] = base.ConvertParamFromString(extrato.lote_id);
            parameter["cnt_id"] = base.ConvertParamFromString(extrato.cnt_id);
       
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectAvariaConteiner", parameter);


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> SelectLacreConteiner(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["cnt_id"] = base.ConvertParamFromString(extrato.cnt_id);
            parameter["cesv_id"] = base.ConvertParamFromString(extrato.cesv_id);

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectLacreConteiner", parameter);


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> ProcExtratoPreFiltro(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["lote_id"] = base.ConvertParamFromString(extrato.lote_id);
            parameter["lote_conhec"] = base.ConvertParamFromString(extrato.lote_conhec);
            parameter["CNPJs"] = base.ConvertParamFromString(extrato.CNPJs);
            parameter["Tipo"] = base.ConvertParamFromString(extrato.Tipo); 
            parameter["TPU_VALOR"] = base.ConvertParamFromInt32(int.Parse(extrato.TPU_VALOR)); 

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.ProcExtratoPreFiltro", parameter);


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> SelectDadosDocSaida(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosDocSaida", base.ConvertParamFromString(extrato.lote_id));


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> SelectDadosCESVSaida(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosCESVSaida", base.ConvertParamFromString(extrato.lote_id));


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> SelectDadosFaturamento(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosFaturamento", base.ConvertParamFromString(extrato.lote_id));


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> SelectDadosDaCda(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosDaCda", base.ConvertParamFromString(extrato.lote_id));


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> SelectDadosApreensao(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.SelectDadosApreensao", base.ConvertParamFromString(extrato.lote_id));


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> ProcPeso(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["cnt_id"] = base.ConvertParamFromString(extrato.cnt_id);
            parameter["cesv_id"] = base.ConvertParamFromString(extrato.cesv_id);

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.ProcExtratoPeso", parameter);


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Extrato> ProcPesoAferido(Extrato extrato)
        {
            _log.Info("Begin Method");
            IList<Extrato> returnValue = null;
            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter["lote_id"] = base.ConvertParamFromString(extrato.lote_id);

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(base.Server(extrato.CompanyName));

                returnValue = mapper.QueryForList<Extrato>("Extrato.ProcPesoAferido", parameter);


            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
    }
}




