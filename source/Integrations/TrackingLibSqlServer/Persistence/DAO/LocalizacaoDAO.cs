﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    public class LocalizacaoDAO : DAOManager<Localizacao>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
       
        public IList<Localizacao> SelectLocalizacaoByLoteID(Localizacao localizacao)
        {
            _log.Info("Begin Method");
            IList<Localizacao> returnValue = null;
            try
            {
                String ServerConnect = String.Empty;
                if (localizacao.CompanyName == "PS BARUERI")
                {
                    ServerConnect = ConfigMapper.SqlMapSQLSERVER_SaraBarueri;
                }
                if (localizacao.CompanyName == "CLIA CAMPINAS")
                {
                    ServerConnect = ConfigMapper.SqlMapSQLSERVER_SaraCampinas;
                }
                if (localizacao.CompanyName == "CLIA SANTOS")
                {
                    ServerConnect = ConfigMapper.SqlMapSQLSERVER_SaraSantos;
                }
                if (localizacao.CompanyName == "CLIA SAO PAULO")
                {
                    ServerConnect = ConfigMapper.SqlMapSQLSERVER_SaraSP;
                }
                ISqlMapper mapper = IBatisMapper.GetMapper(ServerConnect);
                returnValue = mapper.QueryForList<Localizacao>("Localizacao.SelectBylote", localizacao.DI_SLOTE_ID);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
    }
}
