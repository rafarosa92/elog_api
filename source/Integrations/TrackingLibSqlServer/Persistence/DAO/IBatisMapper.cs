﻿using System;
using log4net;
using System.IO;
using System.Reflection;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;
using IBatisNet.DataMapper.SessionStore;
using MBLabs.TrackingLibSqlServer.Transfer;

namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    /// <summary>
    /// This class IBatis Mapper factory which retrieves a desired
    /// ibatis mapper for a certain database.
    /// </summary>
    /// #region Constants


    public class IBatisMapper
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // cache for ibatis mapper
        private static ISqlMapper mapperCache = null;

        #region Private Construtor
        /// <summary>
        /// Private constructor
        /// </summary>
        private IBatisMapper()
        {
        }
        #endregion

        /// <summary>
        /// Get mapper path and file name according the given ibatis mapper type
        /// </summary>
        /// <returns>SQL Mapper</returns>
        public static ISqlMapper GetMapper(String mapper)
        {
            String mapperPathAndName = mapper;
            mapperCache = null;
            if (mapperCache == null && !String.IsNullOrEmpty(mapperPathAndName))
            {
                // initialize ibatis mapper
                ConfigMapper.BancoUsado = mapper;
                DomSqlMapBuilder builder = new DomSqlMapBuilder();

                // get a reference to the current assembly
                Assembly assembly = Assembly.GetExecutingAssembly();
                Stream stream = assembly.GetManifestResourceStream(mapperPathAndName);

                mapperCache = builder.Configure(stream);

                // set session store to hybrid, this is used to run all querys in a single ibatis thread
                mapperCache.SessionStore = new HybridWebThreadSessionStore(mapperCache.Id);
            }

            return mapperCache;
        }
    }
}
