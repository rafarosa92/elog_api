﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    public class LogDAO : DAOManager<Log>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
        public Log InsertLog(Log log)
        {
            _log.Info("Begin Method");
            Log returnValue = null;
            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();
                parameter["Le_Descricao"] = log.Le_Descricao;
                parameter["DI_ID"] = log.DI_ID.ToString();
                parameter["Le_UCriacao"] = log.Le_UCriacao;
                
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForObject<Log>("Log.Insert", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Log> GetLogByID(Log log)
        {
            _log.Info("Begin Method");
            IList<Log> returnValue = null;
            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();
                
                parameter["DI_ID"] = log.DI_ID.ToString();
        
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForList<Log>("Log.SelectByID", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public IList<Log> ProcPendencia(Log log)
        {
            _log.Info("Begin Method");
            IList<Log> returnValue = null;
            try
            {
                String ServerPendencia = String.Empty;
                if (log.UniPendencia == "PS BARUERI")
                {
                    ServerPendencia = ConfigMapper.SqlMapSQLSERVER_SaraBarueri;
                }
                if (log.UniPendencia == "CLIA CAMPINAS")
                {
                    ServerPendencia = ConfigMapper.SqlMapSQLSERVER_SaraCampinas;
                }
                if (log.UniPendencia == "CLIA SANTOS")
                {
                    ServerPendencia = ConfigMapper.SqlMapSQLSERVER_SaraSantos;
                }
                if (log.UniPendencia == "CLIA SAO PAULO")
                {
                    ServerPendencia = ConfigMapper.SqlMapSQLSERVER_SaraSP;
                }
                ISqlMapper mapper = IBatisMapper.GetMapper(ServerPendencia);
                returnValue = mapper.QueryForList<Log>("Log.ProcPendencia", log.pendencia);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
    }
}
