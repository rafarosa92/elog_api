﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections;
using System.Collections.Generic;
using MBLabs.TrackingLibSqlServer;


namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    public class AverbacaoDAO : DAOManager<Averbacao>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public IList<Averbacao> SelectAverbacao()
        {
            _log.Info("Begin Method");

            IList<Averbacao> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForList<Averbacao>("Averbacao.SelectAll", null);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Averbacao> SelectAverbacaoByDate(SelectByDate selectByDate)
        {
            _log.Info("Begin Method");

            IList<Averbacao> returnValue = null;
           

            IDictionary<String, String> parameter = new Dictionary<String, String>();
            parameter.Add("DateStart", base.ConvertParamFromString(selectByDate.DateStart.ToString("MM/dd/yyyy HH:mm:ss")));
            parameter.Add("DateEnd", base.ConvertParamFromString(selectByDate.DateEnd.AddHours(23.99).ToString("MM/dd/yyyy HH:mm:ss")));
            parameter.Add("Uni_ID", base.ConvertParamFromInt32(selectByDate.Uni_ID));
            parameter.Add("DI_UCriacao", selectByDate.DI_UCriacao);

            _log.Info(parameter.Values);
            
            try
            {
                
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForList<Averbacao>("Averbacao.SelectByDate", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public IList<Averbacao> SelectTipoDocumentoById(Int32 Id)
        {
            _log.Info("Begin Method");

            IList<Averbacao> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForList<Averbacao>("AverbacaoTipoDocumento.SelectById", Id);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<Averbacao> SelectDadosDI(Averbacao averbacao)
        {
            _log.Info("Begin Method");

            IList<Averbacao> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForList<Averbacao>("Averbacao.SelectDadosDI", averbacao.DI_ID);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public Averbacao InsertAverbacao(Averbacao averbacao)
        {
            _log.Info("Begin Method");
            Averbacao returnValue = null;
            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();
           
                parameter["DI_UCriacao"] = averbacao.DI_UCriacao;
                parameter["DI_Numero"] = averbacao.DI_Numero;
                parameter["Uni_ID"] = averbacao.Uni_ID.ToString();
                parameter["DI_CNPJ"] = averbacao.DI_CNPJ;
                parameter["DI_QCD_CANAL"] = averbacao.DI_QCD_CANAL;
                parameter["DI_NomeComissaria"] = averbacao.DI_NomeComissaria;
                parameter["DI_Referencia"] = averbacao.DI_Referencia;
                parameter["Di_Observacao"] = averbacao.Di_Observacao; 
                

                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForObject<Averbacao>("Averbacao.Insert", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
               
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public Averbacao AlteraResponsavel(Averbacao averbacao)
        {
            _log.Info("Begin Method");
            Averbacao returnValue = null;
            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();

                parameter["Di_Responsavel"] = averbacao.DI_UCriacao;
                parameter["DI_ID"] = averbacao.DI_ID.ToString();

                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForObject<Averbacao>("Averbacao.AlteraResponsavel", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);

                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public Averbacao CancelarDI(Averbacao averbacao)
        {
            _log.Info("Begin Method");
            Averbacao returnValue = null;
            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();

                parameter["DI_ID"] = averbacao.DI_ID.ToString();

                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForObject<Averbacao>("Averbacao.CancelarDI", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);

                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        public Averbacao ReprocessarDI(Averbacao averbacao)
        {
            _log.Info("Begin Method");
            Averbacao returnValue = null;
            try
            {
                IDictionary<String, String> parameter = new Dictionary<String, String>();

                parameter["DI_ID"] = averbacao.DI_ID.ToString();

                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);
                returnValue = mapper.QueryForObject<Averbacao>("Averbacao.ReprocessarDI", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);

                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        #endregion
    }
}