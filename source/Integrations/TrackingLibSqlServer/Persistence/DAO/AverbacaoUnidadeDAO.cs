﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.TrackingLibSqlServer.Exception;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections;
using System.Collections.Generic;
using MBLabs.TrackingLibSqlServer;


namespace MBLabs.TrackingLibSqlServer.Persistence.DAO
{
    public class AverbacaoUnidadeDAO 
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public AverbacaoUnidade SelectPathFile(Int32 Uni_ID)
        {
            _log.Info("Begin Method");

            AverbacaoUnidade returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);

                returnValue = mapper.QueryForObject<AverbacaoUnidade>("AverbacaoUnidade.SelectByUniID", Uni_ID);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        public IList<AverbacaoUnidade> SelectParamComprovante(Int32 Uni_ID)
        {
            _log.Info("Begin Method");

            IList<AverbacaoUnidade> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper(ConfigMapper.SqlMapSQLSERVER_Tracking);

                returnValue = mapper.QueryForList<AverbacaoUnidade>("AverbacaoUnidade.SelecrParamComprovante", Uni_ID);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        #endregion
    }
}