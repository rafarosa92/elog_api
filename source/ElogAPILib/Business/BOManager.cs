﻿using log4net;
using MBLabs.ElogAPILib.Exception;
using MBLabs.ElogAPILib.Persistence.DAO;
using MBLabs.ElogAPILib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.ElogAPILib.Business
{
    /// <summary>
    /// Class responsible for setting all business generic methods
    /// </summary>
    public class BOManager<T> where T : BaseEntity
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Save or Update
        /// <summary>
        /// Method responsible for saving or update generics base entity
        /// </summary>
        /// <param name="dto">dto</param>
        /// <param name="insertByUserId">insertByUserId</param>
        /// <returns>entity object extension</returns>
        public T SaveOrUpdate(T dto, Int32 insertByUserId)
        {
            _log.Info("Begin Method");

            try
            {
                DAOManager<T> daoManager = new DAOManager<T>();

                // check if is an update
                if (dto.Id > 0)
                {
                    // perform an update
                    dto.DtUpdate = DateTime.Now;
                    dto.UpdateByUserId = insertByUserId;

                    // bug fix (when values are null)
                    dto.InsertByUserId = dto.InsertByUserId == 0 ? dto.UpdateByUserId : dto.InsertByUserId;
                    dto.DtInsert = DateTime.MinValue == dto.DtInsert ? DateTime.Now : dto.DtInsert;

                    daoManager.Update(dto);
                }
                else
                {
                    // perform an insert
                    dto.DtInsert = DateTime.Now;
                    dto.DtUpdate = DateTime.Now;
                    dto.UpdateByUserId = insertByUserId;
                    dto.InsertByUserId = insertByUserId;
                    dto.Id = daoManager.Insert(dto);
                }
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return dto;
        }
        #endregion

        #region Get Methods
        /// <summary>
        /// Method responsible getting 
        /// </summary>
        /// <param name="id">id</param>
        /// <returns>entity generics</returns>
        public T GetById(Int32 id)
        {
            _log.Info("Begin Method");

            T returnValue = null;

            try
            {
                DAOManager<T> daoManager = new DAOManager<T>();
                returnValue = daoManager.SelectById(id);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        /// <summary>
        /// Method responsible getting all
        /// </summary>
        /// <returns>entity generics list</returns>
        public IList<T> GetAll()
        {
            _log.Info("Begin Method");

            IList<T> returnList = null;

            try
            {
                DAOManager<T> daoManager = new DAOManager<T>();
                returnList = daoManager.SelectAll();
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnList;
        }

        /// <summary>
        /// Method responsible for getting by search parameter
        /// </summary>
        /// <param name="searchParameter">searchParameter</param>
        /// <param name="countAll">countAll</param>
        /// <returns>entity base</returns>
        public IList<T> GetBySearchParameter(Object searchParameter, out Int32 countAll)
        {
            _log.Info("Begin Method");

            IList<T> returnList = null;

            try
            {
                DAOManager<T> daoManager = new DAOManager<T>();
                returnList = daoManager.SelectBySearchParameter(searchParameter, out countAll);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnList;
        }
        #endregion

        #region Delete Methods
        /// <summary>
        /// Method responsible for delete entity
        /// </summary>
        /// <param name="id">entity id</param>
        public void Delete(Int32 id)
        {
            _log.Info("Begin Method");

            try
            {
                DAOManager<T> daoManager = new DAOManager<T>();
                T dto = daoManager.SelectById(id);
                dto.IsActive = false;
                daoManager.Update(dto);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");
        }
        #endregion
    }
}