﻿using log4net;
using MBLabs.ElogAPILib.Exception;
using MBLabs.ElogAPILib.Mechanism;
using MBLabs.ElogAPILib.Persistence.DAO;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.ElogAPILib.Business
{
    public class SecurityTokenBO : BOManager<SecurityToken>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly String urlSystem = "/api/securitytoken/generatetoken";
        #endregion

        public SecurityToken Authenticate(String APIkey, String username, String password, String requestIP)
        {
            _log.Info("Begin Method");

            SecurityToken returnValue = null;

            try
            {
                //Check user exist and is valid
                UserBO userBO = new UserBO();
                User user = userBO.Authenticate(username, password);

                SecuritySystemBO securitySystemBO = new SecuritySystemBO();
                SecuritySystem securitySystem = securitySystemBO.getByAPIkey(APIkey);

                if(!securitySystemBO.ValidateAccess(requestIP, APIkey, urlSystem))
                {
                    throw new BusinessException("MappingTable blocked the access from " + requestIP);
                }

                //generate new token
                SecurityTokenDAO securityTokenDAO = new SecurityTokenDAO();
                returnValue = new SecurityToken();
                returnValue.IsActive = true;
                returnValue.IP = requestIP;
                returnValue.Email = user.Email;
                returnValue.SecuritySystemId = securitySystem.Id;
                returnValue.Token = SecurityManager.GenerateToken();
                returnValue.DtTokenExpiration = DateTime.Now.AddDays(1);
                returnValue.DtLastAccess = DateTime.Now;

                returnValue = base.SaveOrUpdate(returnValue, user.Id);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public SecurityToken GetSecurityToken(String token)
        {
            _log.Info("Begin Method");

            SecurityToken returnValue = null;

            try
            {
                SecurityTokenDAO securityTokenDAO = new SecurityTokenDAO();
                returnValue = securityTokenDAO.SelectByToken(token);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public void ValidateToken(String tokenAuth)
        {
            _log.Info("Begin Method");
            try
            {
                SecurityTokenDAO securityTokenDAO = new SecurityTokenDAO();
                SecurityToken securityToken = securityTokenDAO.SelectByToken(tokenAuth);

                if ( securityToken != null )
                {
                    if (securityToken.DtTokenExpiration <= DateTime.Now)
                    {
                        throw new AuthenticationFailureException("Token was expired");
                    }
                }
                else
                {
                    throw new AuthenticationFailureException("Token was not found");
                }
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");
        }
    }
}