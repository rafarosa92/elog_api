﻿using log4net;
using MBLabs.ElogAPILib.Exception;
using MBLabs.ElogAPILib.Mechanism;
using MBLabs.ElogAPILib.Persistence.DAO;
using MBLabs.ElogAPILib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.ElogAPILib.Business
{
    public class UserBO : BOManager<SecurityToken>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public User Authenticate(String username, String password)
        {
            _log.Info("Begin Method");

            User returnValue;
            try
            {
                // perform search user by email
                UserDAO userDAO = new UserDAO();
                returnValue = userDAO.SelectByLoginAndPassword(username, password);

                if (returnValue == null)
                {
                    throw new AuthenticationFailureException("Access not found");
                }
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }
            _log.Info("End Method");

            return returnValue;
        }


        // Vou ter que fazer uma função de validar token e uma de gerar token --- mesmo sabendo que token vai ficar em outra tabela
        /*
        public void ValidateToken(String tokenAuth)
        {
            _log.Info("Begin Method");

            try
            {
                SecurityTokenDAO securityTokenDAO = new SecurityTokenDAO();
                SecurityToken securityToken = securityTokenDAO.SelectByToken(tokenAuth);

                if (securityToken != null)
                {
                    if (securityToken.DtTokenExpiration <= DateTime.Now)
                    {
                        throw new AuthenticationFailureException("Token was expired");
                    }
                }
                else
                {
                    throw new AuthenticationFailureException("Token was not found");
                }
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");
        }
        */
    }
}