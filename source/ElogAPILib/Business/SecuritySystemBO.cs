﻿using log4net;
using MBLabs.ElogAPILib.Exception;
using MBLabs.ElogAPILib.Mechanism;
using MBLabs.ElogAPILib.Persistence.DAO;
using MBLabs.ElogAPILib.Transfer;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Caching;
using MBLabs.ElogAPILib.Configuration;
using System.Runtime.Caching;

namespace MBLabs.ElogAPILib.Business
{
    public class SecuritySystemBO : BOManager<SecuritySystem>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public Boolean ValidateAccess(String requestIP, String APIKey, String requestUrl)
        {
            Boolean returnValue = true;

            _log.Info("Begin Method");

            try
            {
                MappingTableItem mappingTable = this.GetMappingTable().Where(x => (x.IPAllowed == requestIP || x.IPAllowed == "*") && x.APIKey == APIKey && x.SystemUrl == requestUrl).FirstOrDefault();
                returnValue = mappingTable == null ? false : true;
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }

            _log.Info("End Method");

            return returnValue;
        }

        public SecuritySystem getByAPIkey(String apikey)
        {
            SecuritySystem returnValue = null;

            _log.Info("Begin Method");
            try
            {
                SecuritySystemDAO securitySystemDAO = new SecuritySystemDAO();
                returnValue = securitySystemDAO.SelectByAPIKey(apikey);
            }
            catch (PersistenceException pex)
            {
                throw new BusinessException(pex);
            }
            _log.Info("End Method");

            return returnValue;
        }

        public static void RefreshMappingTableCache()
        {
            try
            {
                IList<MappingTableItem> tableItem = new List<MappingTableItem>();
                SecuritySystemBO securitySystemBO = new SecuritySystemBO();
                IList<SecuritySystem> securitySystemList = securitySystemBO.GetAll();

                SecuritySystemAclDAO aclDAO = new SecuritySystemAclDAO();
                foreach (SecuritySystem securitySystem in securitySystemList)
                {
                    IList<SecuritySystemAcl> aclList = aclDAO.SelectBySystemId(securitySystem.Id);
                    foreach (SecuritySystemAcl item in aclList)
                    {
                        tableItem.Add(new MappingTableItem()
                        {
                            APIKey = securitySystem.APIKey,
                            SystemName = securitySystem.SystemName,
                            SystemUrl = securitySystem.SystemUrl,
                            IPAllowed = item.IPAllowed
                        });
                    }

                }

                MemoryCache memoryCache = MemoryCache.Default;
                memoryCache.AddOrGetExisting("ApiElogSecurityMapping", tableItem, DateTimeOffset.UtcNow.AddDays(30));
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        public IList<MappingTableItem> GetMappingTable()
        {
            MemoryCache memoryCache = MemoryCache.Default;
            return (IList<MappingTableItem>)memoryCache.Get("ApiElogSecurityMapping");
        }
    }

    public class MappingTableItem
    {
        public String APIKey { get; set; }
        public String SystemName { get; set; }
        public String SystemUrl { get; set; }
        public String IPAllowed { get; set; }
    }
}