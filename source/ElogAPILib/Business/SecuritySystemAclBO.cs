﻿using log4net;
using MBLabs.ElogAPILib.Exception;
using MBLabs.ElogAPILib.Mechanism;
using MBLabs.ElogAPILib.Persistence.DAO;
using MBLabs.ElogAPILib.Transfer;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Caching;
using IBatisNet.DataMapper;

namespace MBLabs.ElogAPILib.Business
{
    public class SecuritySystemAclBO : BOManager<SecuritySystemAcl>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion
    }
}