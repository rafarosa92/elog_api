﻿namespace MBLabs.ElogAPILib.Enumerator
{
    public enum ResultStatus
    {
        SUCCESS = 1,
        SUCCESS_WARNING = 2,
        ERROR = 3
    }
}
