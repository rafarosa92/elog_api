﻿using MBLabs.ElogAPILib.Util;

namespace MBLabs.ElogAPILib.Enumerator
{
    public enum UserType
    {
        [EnumDescription("Administrador")]
        ADMINISTRATOR = 1,

        [EnumDescription("Gerente")]
        MANAGER = 2,

        [EnumDescription("Operador")]
        OPERATOR = 3,

        [EnumDescription("Promotor")]
        PROMOTER = 4,
    }
}
