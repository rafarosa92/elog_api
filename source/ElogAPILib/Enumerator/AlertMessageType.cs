﻿public enum AlertMessageType
{
    ERROR,
    INFO,
    WARN,
    SUCCESS
}