﻿using System;

namespace MBLabs.ElogAPILib.Transfer
{
    public class SecuritySystem : BaseEntity
    {
        public String APIKey { get; set; }
        public String SystemName { get; set; }
        public String SystemUrl { get; set; }
    }
}