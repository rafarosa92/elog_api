﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Util;
using Newtonsoft.Json;
using System;

namespace MBLabs.ElogAPILib.Transfer
{
    public class User : BaseEntity
    {
        public String Username { get; set; }
        public String Login { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public UserType ProfileType { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime DtLastAccess { get; set; }
    }
}