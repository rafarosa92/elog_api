﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MBLabs.ElogAPILib.Enumerator;

namespace MBLabs.ElogAPILib.Transfer.SearchParameter
{
    public class UserMobileSearchParameter : BaseSearchParameter
    {
        public String Name { get; set; }
        public String Email { get; set; }
        public String CPF { get; set; }
        public String RG { get; set; }
        public DateTime? DtBegin { get; set; }
        public DateTime? DtEnd { get; set; }
        public Boolean IsActive { get; set; }
        public Int32? StoreId { get; set; }
    }
}
