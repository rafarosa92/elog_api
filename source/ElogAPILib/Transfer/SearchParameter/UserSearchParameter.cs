﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MBLabs.ElogAPILib.Enumerator;

namespace MBLabs.ElogAPILib.Transfer.SearchParameter
{
    public class UserSearchParameter : BaseSearchParameter
    {
        public String Name { get; set; }
    }
}
