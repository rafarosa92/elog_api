﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.ElogAPILib.Transfer.SearchParameter
{
    public class BaseSearchParameter
    {
        public static Int32 DefaultPageSize = 10;

        private Int32 pageSize;
        private Int32 currentPage;

        public Int32 CurrentPage { get { return currentPage; } set { currentPage = value; } }
        public Int32 PageSize { get { return (pageSize == 0 ? DefaultPageSize : pageSize); } set { pageSize = value; } }
        public Int32 PageStart { get { return currentPage * PageSize; } }
        public Int32 PageEnd { get { return (currentPage + 1) * PageSize; } }
    }
}
