﻿using MBLabs.ElogAPILib.Util;
using Newtonsoft.Json;
using System;

namespace MBLabs.ElogAPILib.Transfer
{
    public class SecurityToken : BaseEntity
    {
        public String Token { get; set; }
        public String IP { get; set; }
        public String Email { get; set; }
        public Int32 SecuritySystemId { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime DtTokenExpiration { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime DtLastAccess { get; set; }
 
    }
}