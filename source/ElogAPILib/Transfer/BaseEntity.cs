﻿using System;
using Newtonsoft.Json;
using MBLabs.ElogAPILib.Util;

namespace MBLabs.ElogAPILib.Transfer
{
    public class BaseEntity
    {
        public Int32 Id { get; set; }
        //[JsonIgnore]
        public Int32 InsertByUserId { get; set; }
        //[JsonIgnore]
        public Int32 UpdateByUserId { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime DtInsert { get; set; }
        [JsonIgnore]
        public DateTime DtUpdate { get; set; }
        [JsonIgnore]
        public Boolean IsActive { get; set; }
    }
}
