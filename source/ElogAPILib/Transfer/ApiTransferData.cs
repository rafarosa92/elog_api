﻿using System;

namespace MBLabs.ElogAPILib.Transfer
{
    public class ApiTransferData<T>
    {
        public String ResultStatus { get; set; }
        public String ResultStatusMessage { get; set; }
        public T ReturnData { get; set; }
        public String ReturnValue { get; set; }
    }
}
