﻿using System;

namespace MBLabs.ElogAPILib.Transfer
{
    public class SecuritySystemAcl : BaseEntity
    {
        public Int32 SystemId { get; set; }
        public String IPAllowed { get; set; }
    }
}