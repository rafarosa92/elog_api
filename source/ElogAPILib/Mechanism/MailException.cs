using System;

namespace MBLabs.ElogAPILib.Exception
{
    [Serializable]
    public class MailException : ApplicationException
    {
         public MailException(System.Exception innerExc)
            : base(innerExc.GetType().Name, innerExc)
        {
        }

         public MailException(String innerExc)
            : base(innerExc)
        {
        }
    }
}