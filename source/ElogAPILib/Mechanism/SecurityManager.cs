﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace MBLabs.ElogAPILib.Mechanism
{
    public class SecurityManager
    {
        /// <summary>
        /// Method responsible for calculating hash MD5 using hex array string
        /// </summary>
        /// <param name="dataToBeHash">dataToBeHash</param>
        /// <returns>hash value (hex)</returns>
        public static String CalculateMD5Hash(String dataToBeHash)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = MD5.Create();
            Byte[] inputBytes = Encoding.UTF8.GetBytes(dataToBeHash);
            Byte[] hash = md5.ComputeHash(inputBytes);

            return BitConverter.ToString(hash).Replace("-", String.Empty).ToUpper();
        }

        /// <summary>
        /// Method responsible for generating token based on guid
        /// </summary>
        /// <returns>String token</returns>
        public static String GenerateToken()
        {
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray());
        }
    }
}
