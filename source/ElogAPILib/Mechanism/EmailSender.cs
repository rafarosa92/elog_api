﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading;
using MBLabs.ElogAPILib.Exception;

namespace MBLabs.ElogAPILib.Mechanism
{
    public class EmailSender
    {
        /// <summary>
        /// Method responsible for sending email using gmail host
        /// </summary>
        /// <param name="to">Email to be send</param>
        /// <param name="subjectText">Subject email text</param>
        /// <param name="bodyMessage">Body message</param>
        public static void SendEmail(String to, String cc, String subjectText, String bodyMessage)
        {
            try
            {
                MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings["MailUserName"], ConfigurationManager.AppSettings["MailTitle"]);
                MailAddress toAddress = new MailAddress(to,to);

                //String fromPassword = ConfigurationManager.AppSettings["MailPassword"];
                String subject = subjectText;
                String body = bodyMessage;

                // configure stmp client with host port and credetials
                SmtpClient smtp = new SmtpClient();

                smtp.Host = "mail.eloglogistica.com.br";
                smtp.Port = 25;
                smtp.EnableSsl = false;
                System.Net.NetworkCredential SMTPUsuario = new System.Net.NetworkCredential("smtpuser", "123@mudar");
                smtp.Timeout = 50000;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = SMTPUsuario;

           

                // send mail
                using (MailMessage message = new MailMessage (fromAddress, toAddress) { Subject = subject, Body = body, IsBodyHtml = true })
                {
                    if (cc != null)
                    {
                        message.CC.Add(cc);
                    
                    }

                     smtp.Send(message);
                   
                }
            }
            catch (System.Exception ex)
            {
                Console.Write(ex.Message);
                throw new MailException(ex.Message);
                
            }
        }

        public static bool IsValidEmail(string email)
        {
            Boolean isValid = false;

            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            if (rg.IsMatch(email))
            {
                isValid = true;
            }
            else
            {
                isValid = false;
            }

            return isValid;
        }
    }
}

