﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.ElogAPILib.Exception;
using MBLabs.ElogAPILib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.ElogAPILib.Persistence.DAO
{
    public class SecurityTokenDAO : DAOManager<SecurityToken>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        /// <summary>
        /// Method responsible for selecting token
        /// </summary>
        /// <param name="token">token</param>
        /// <returns>SecurityToken object</returns>
        public SecurityToken SelectByToken(String token)
        {
            _log.Info("Begin Method");

            SecurityToken returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<SecurityToken>("SecurityToken.SelectByToken", token);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        #endregion
    }
}