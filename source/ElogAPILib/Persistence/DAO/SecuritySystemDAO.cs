﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.ElogAPILib.Exception;
using MBLabs.ElogAPILib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.ElogAPILib.Persistence.DAO
{
    public class SecuritySystemDAO : DAOManager<SecuritySystem>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal string ValidateAccess(string requestIP, string apiToken, string requestUrl)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Select Methods
        /// <summary>
        /// Method responsible for selecting by API key
        /// </summary>
        /// <param name="token">token</param>
        /// <returns>SecuritySystem object</returns>
        public SecuritySystem SelectByAPIKey(String APIkey)
        {
            _log.Info("Begin Method");
            SecuritySystem returnValue = null;
            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<SecuritySystem>("SecuritySystem.SelectByAPIkey", APIkey);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }
            _log.Info("End Method");
            return returnValue;
        }
        #endregion
    }
}