﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.ElogAPILib.Exception;
using MBLabs.ElogAPILib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.ElogAPILib.Persistence.DAO
{
    public class UserDAO : DAOManager<SecurityToken>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        
        /// <summary>
        /// Method responsible for selecting by username and password
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>SecurityToken object</returns>
        public User SelectByLoginAndPassword(String username, String password)
        {
            _log.Info("Begin Method");

            User returnValue = null;

            try
            {
                // perform a query search
                IDictionary<String, String> parameter = new Dictionary<String, String>();
                parameter["login"] = username;
                parameter["password"] = password;

                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForObject<User>("User.SelectByLoginAndPassword", parameter);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        #endregion
    }
}