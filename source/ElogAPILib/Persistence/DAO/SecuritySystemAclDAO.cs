﻿using IBatisNet.DataMapper;
using log4net;
using MBLabs.ElogAPILib.Exception;
using MBLabs.ElogAPILib.Transfer;
using System;
using System.Collections.Generic;

namespace MBLabs.ElogAPILib.Persistence.DAO
{
    public class SecuritySystemAclDAO : DAOManager<SecuritySystemAcl>
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        #region Select Methods
        public IList<SecuritySystemAcl> SelectBySystemId(Int32 systemId)
        {
            _log.Info("Begin Method");

            IList<SecuritySystemAcl> returnValue = null;

            try
            {
                ISqlMapper mapper = IBatisMapper.GetMapper();
                returnValue = mapper.QueryForList<SecuritySystemAcl>("SecuritySystemAcl.SelectBySystemId", systemId);
            }
            catch (System.Exception ex)
            {
                _log.Error(ex);
                throw new PersistenceException(ex);
            }

            _log.Info("End Method");

            return returnValue;
        }
        #endregion
    }
}