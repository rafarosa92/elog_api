using MBLabs.ElogAPILib.Exception;
using System.Web.Mvc;

namespace MBLabs.ElogAPIWeb.Controllers
{
    public class AccountController : Controller
    {
        #region Constaints
        public const string MAIL_TITLE_REGISTRATION = "[ Mundo Irys ] Conta registrada com sucesso";
        public const string MAIL_TITLE_PASSWORD_RECOVERY = "[ Mundo Irys ] Sua senha foi alterada";
        public const string MAIL_SUBJECT_REGISTRATION = "Olá {0},<br/><br/>Seja muito bem vindo ao sistema de controle de pedidos da Mundo Irys. Realizamos o seu cadastro com sucesso! Seu usuário para realizar o acesso no nosso website será este endereço de e-mail <b>{1}</b>. Aproveite todas as nossas funcionalidades e entre em contato conosco caso deseje solicitar alguma melhoria.<br/><br/>Esta mensagem é automatica, favor no responde-la.<br/>Qualquer dúvida favor entrar em contato com contato@mblabs.com.br";
        public const string MAIL_SUBJECT_PASSWORD_RECOVERY = "Olá {0},<br/><br/>Realizamos o processo de recuperação de senha para seu usuário {1} e sua nova senha é <b>{2}</b><br/><br/>Esta mensagem é automatica, favor no responde-la.<br/>Qualquer dúvida favor entrar em contato com contato@mblabs.com.br";
        #endregion

        #region Login
        [HttpGet]
        public ActionResult Login()
        {
            TempData["DisableMenu"] = true;
            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection colletion)
        {
            try
            {
            }
            catch (AuthenticationFailureException af)
            {
            }

            return View();
        }
        #endregion

        #region Logout
        [HttpGet]
        public ActionResult Logout()
        {
            return base.RedirectToRoute("AccountLogin");
        }

        [HttpPost]
        public ActionResult Logout(FormCollection colletion)
        {
            return base.View();
        }
        #endregion

        #region Password Recovery
        [HttpPost]
        public ActionResult PasswordRecovery(FormCollection collection)
        {
            return View();
        }

        [HttpGet]
        public ActionResult PasswordRecovery()
        {
            TempData["DisableMenu"] = true;

            return View();
        }
        #endregion
    }
}