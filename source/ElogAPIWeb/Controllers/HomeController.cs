using MBLabs.ElogAPILib.Enumerator;
using System.Web.Mvc;

namespace MBLabs.ElogAPIWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        #region -- Robots() Method --
        public ActionResult Robots()
        {
            Response.ContentType = "text/plain";
            return View();
        }
        #endregion
    }
}