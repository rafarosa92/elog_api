﻿using FluentScheduler;
using log4net;
using MBLabs.ElogAPILib.Business;
using System;
using System.Web.Hosting;

namespace MBLabs.ElogAPIWeb.Jobs
{
    public class ImportJob : ITask, IRegisteredObject
    {
        #region Constants
        private readonly object _lock = new object();
        private bool _shuttingDown;
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        public ImportJob()
        {
            // Register this task with the hosting environment.
            // Allows for a more graceful stop of the task, in the case of IIS shutting down.
            HostingEnvironment.RegisterObject(this);
        }

        public void Execute()
        {
            lock (_lock)
            {
                if (_shuttingDown)
                {
                    return;
                }

                try
                {
                    SecuritySystemBO.RefreshMappingTableCache();
                }
                catch (Exception ex)
                {
                    _log.Error(ex.Message);
                    throw;
                }
            }
        }

        public void Stop(bool immediate)
        {
            // Locking here will wait for the lock in Execute to be released until this code can continue.
            lock (_lock)
            {
                _shuttingDown = true;
            }

            HostingEnvironment.UnregisterObject(this);
        }
    }
}