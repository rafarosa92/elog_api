﻿using FluentScheduler;

namespace MBLabs.ElogAPIWeb.Jobs
{
    public class TaskManagerJobs : Registry
    {
        public TaskManagerJobs()
        {
            Schedule<ImportJob>().ToRunNow().AndEvery(1).Minutes();
        }
    }
}