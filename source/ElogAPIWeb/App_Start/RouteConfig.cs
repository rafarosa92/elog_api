using System.Web.Mvc;
using System.Web.Routing;

namespace MBLabs.ElogAPIWeb
{
    /// <summary>
    /// Class responsible for configurate all routes from elog application
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Method responsible for registering all elog
        /// </summary>
        /// <param name="routes">routes</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            // free routes
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.axdx/{*pathInfo}");
            routes.IgnoreRoute("favicon.ico");
            
            // robots txt
            routes.MapRoute("Robots.txt",
                "robots.txt",
                new { controller = "Home", action = "Robots" });
            
            // default route
            routes.MapRoute(
                 "Default", // Route name
                 "{controller}/{action}/{id}", // URL with parameters
                 new { controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
                 new string[] { "MBLabs.ElogAPIWeb.Controllers" }
            );
        }
    }
}