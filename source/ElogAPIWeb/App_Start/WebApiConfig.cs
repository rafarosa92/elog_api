using System.Web.Http;

namespace MBLabs.ElogAPIWeb
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
            name: "ControllersApi",
             routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Web API routes
            //config.MapHttpAttributeRoutes();
        }
    }
}