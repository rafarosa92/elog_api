﻿using System.Web.Optimization;

namespace MBLabs.ElogAPIWeb
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery/jquery-{version}.js",
                       "~/Scripts/jquery/jquery.mask.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery/jquery.unobtrusive*",
                        "~/Scripts/jquery/jquery.validate*"));

            // jquery core and ui (script)
            bundles.Add(new ScriptBundle("~/bundles/script/jquery").Include(
                    "~/Scripts/jquery/jquery-2.0.3.js",
                    "~/Scripts/jquery/jquery-ui-1.10.4.js"
                ));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr/modernizr-*"));

            // mvp ready template (script)
            bundles.Add(new ScriptBundle("~/bundles/script/mvpready").Include(

                /* Core JS */
                "~/Content/themes/mvpready/scripts/bootstrap.js",

                /* App JS */
                "~/Content/themes/mvpready/scripts/mvpready-core.js",
                "~/Content/themes/mvpready/scripts/mvpready-admin.js",
                "~/Content/themes/mvpready/scripts/validation.js",
                "~/Content/themes/mvpready/scripts/parsley.js",
                
                /* Moment */
                "~/Content/moment/moment.js",
                "~/Content/moment/moment-with-locales.js",

                /* Date Picker */
                "~/Content/datepicker/js/main.js",

                /* Date and Time Picker */
                "~/Content/datetimepicker/js/jquery.datetimepicker.full.min.js",

                /* Modal Pop Up */
                "~/Content/themes/mvpready/scripts/jquery.magnific-popup.js",

                /* Auto Complete */
                "~/Content/themes/mvpready/scripts/jquery.typeahead.js",

                /* Defaults */
                "~/Scripts/main.js"
            ));

            // webcam js
            bundles.Add(new ScriptBundle("~/bundles/script/webcam").Include(
                    "~/Content/webcam/webcam.js"
                ));

            // loader script
            bundles.Add(new ScriptBundle("~/bundles/script/blockUi").Include(
                "~/Content/blockui/blockUi.js"
            ));

            // pace script
            bundles.Add(new ScriptBundle("~/bundles/script/pace").Include(
                "~/Content/pace/pace.js"
            ));

            // notify
            bundles.Add(new ScriptBundle("~/bundles/script/notify").Include(
                "~/Content/notify/notify.js"
            ));

            // orgchart
            bundles.Add(new ScriptBundle("~/bundles/script/orgchart").Include(
                "~/Content/orgchart/jquery.orgchart.js"
            ));

            // mvp ready dashboard components (script)
            bundles.Add(new ScriptBundle("~/bundles/script/mvpready-dashboard").Include(
               "~/Content/themes/mvpready/scripts/jquery.flot.js",
               "~/Content/themes/mvpready/scripts/jquery.flot.tooltip.js",
               "~/Content/themes/mvpready/scripts/jquery.flot.pie.js",
               "~/Content/themes/mvpready/scripts/jquery.flot.resize.js",
               "~/Content/themes/mvpready/scripts/line.js",
               "~/Content/themes/mvpready/scripts/pie.js",
               "~/Content/themes/mvpready/scripts/auto.js"
            ));

            // mvp ready template (style)
            bundles.Add(new StyleBundle("~/bundles/style/mvpready").Include(
                "~/Content/default.css",
                "~/Content/themes/mvpready/bootstrap.css",
                "~/Content/themes/mvpready/mvpready-admin.css",
                "~/Content/datepicker/css/main.css",
                "~/Content/datetimepicker/css/jquery.datetimepicker.min.css",
                "~/Content/themes/mvpready/magnific-popup.css"
            ));

            bundles.Add(new StyleBundle("~/bundles/style/pace").Include(
                "~/Content/pace/themes/red/pace-theme-barber-shop.css"
            ));

            bundles.Add(new StyleBundle("~/bundles/style/orgchart").Include(
                "~/Content/orgchart/jquery.orgchart.css"
            ));

        }
    }
}