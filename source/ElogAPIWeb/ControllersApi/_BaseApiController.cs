﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using System.Web;
using System.Globalization;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPILib.Business;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    public class _BaseApiController : ApiController
    {
        protected string GetClientIP()
        {
            var request = Request;
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            else if (HttpContext.Current != null)
            {
                return HttpContext.Current.Request.UserHostAddress;
            }
            else
            {
                return null;
            }
        }

        protected Header GetHeader()
        {
            Header returnValue = new Header();
            String headerAuthorization = Request.Headers.Authorization == null ? "" : Request.Headers.Authorization.Parameter;
            String Authtoken = String.IsNullOrEmpty(headerAuthorization) == true ? "" : headerAuthorization.Substring(headerAuthorization.IndexOf(" ", StringComparison.Ordinal) + 1);
            returnValue.AuthToken = String.IsNullOrEmpty(Authtoken) == true? "": Authtoken.Replace("token=", String.Empty).Replace("\"", String.Empty);
            returnValue.APIKey = Request.Headers.GetValues("api_key").FirstOrDefault();
            return returnValue;
        }

        /// <summary>
        /// Method responsible for get secret date from token string
        /// </summary>
        /// <param name="token">token string with secret date</param>
        /// <returns>secret date</returns>
        private Nullable<DateTime> GetSecretDate(String token)
        {
            DateTime datetime;
            try
            {
                if (DateTime.TryParseExact(token.Substring(1, 14), "MMHHyyyymmddss", CultureInfo.InvariantCulture, DateTimeStyles.None, out datetime))
                {
                    return datetime;
                }
            }
            catch (Exception)
            {
                throw new Exception("Token has invalid format");
            }

            return null;
        }
    }
    
    public class Header{
        public String AuthToken { get; set; }
        public String APIKey { get; set; }
    }
}