﻿
using MBLabs.ElogAPILib.Mechanism;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.IO;
using MBLabs.TrackingLib.Enumerator;
using MBLabs.ElogAPILib.Business;
using System.Net;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/tracking")]
    public class UserController : _BaseApiController
    {
        [HttpGet]
        [Route("authentication")]
        [UserAuthentication]
        public ApiTransferData<User> Authentication(String username, String password)
        {
            ApiTransferData<User> returnValue = new ApiTransferData<User>();

            try
            {
                UserBO userBO = new UserBO();
                User securityToken = userBO.Authenticate(username, password);
                

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Authentication was sucessfully";
                returnValue.ReturnData = securityToken;
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpGet]
        [Route("listuseraccess")]
        [UserAuthentication]
        public ApiTransferData<IList<AccessProfile>> ListUserAccess(Int32 UserId)
        {
            ApiTransferData<IList<AccessProfile>> returnValue = new ApiTransferData<IList<AccessProfile>>();

            try
            {
                AccessProfileBO accessProfileBO = new AccessProfileBO();

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = accessProfileBO.ListUserAccess(UserId);

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Create")]
        public ApiTransferData<User> Create([FromBody] User user)
        {
            ApiTransferData<User> returnValue = new ApiTransferData<User>();

            try
            {
                
                Company companyBO = new CompanyBO().GetCompanyByName(user.Recinto);
                UserBO userBO = new UserBO();
                user.RecintoID = companyBO.Id;
                User userInserted = userBO.Save(user);




                if (userInserted.P_result > 0)
                {
                    returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                    returnValue.ResultStatusMessage = "User was created sucessfully";
                    returnValue.ReturnData = userInserted;


                    String email = new StreamReader(@"C:\inetpub\elog_api\Views\emails\email.html").ReadToEnd();
                    email = email.Replace("#TITLE#", "Cadastro de novo usuário");
                    email = email.Replace("#ID#", userInserted.P_result.ToString());
                    email = email.Replace("#NAME#", user.Name.ToString());
                    email = email.Replace("#EMPRESA#", user.Company.ToString());
                    email = email.Replace("#EMAIL#", user.Email.ToString());
                    email = email.Replace("#TELEFONE#", user.Phone.ToString());

                    EmailSender.SendEmail(companyBO.KeeperEmail, "rafael@sidedoor.com.br", "Cadastro de Usuário - E-Tracking", email);

                }
                else
                {
                    returnValue.ResultStatus = ResultStatus.SUCCESS_WARNING.ToString();
                    returnValue.ResultStatusMessage = "Error Create User";
                    returnValue.ReturnData = userInserted;

                }

            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }
            return returnValue;
        }

        [HttpGet]
        [Route("RequestResetPassw")]
        public ApiTransferData<SecurityToken> RequestResetPassw(String email)
        {
            ApiTransferData<SecurityToken> returnValue = new ApiTransferData<SecurityToken>();
            try
            {
                UserBO userBO = new UserBO();
                User user = userBO.SelectByEmail(email);

                // genarete token authenticate
                SecurityTokenBO securityTokenBO = new SecurityTokenBO();
                SecurityToken securityToken = securityTokenBO.Authenticate(base.GetHeader().APIKey, "TRACKING", "e-tracking", base.GetClientIP());

                // build return
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "URL Generator to reset password has successfully";
                returnValue.ReturnData = null;
                returnValue.ReturnValue = @"https://etracking.eloglogistica.com.br/new/view/resetPassw.php/?" + "id_token=" + user.Id + "&" + "token=" + WebUtility.UrlEncode(securityToken.Token);

                String sendemail = new StreamReader(@"C:\inetpub\elog_api\Views\emails\newPassword.html").ReadToEnd();
                sendemail = sendemail.Replace("#URL_PASS#", returnValue.ReturnValue);
                EmailSender.SendEmail(email, null, "E-Tracking - Trocar Senha", sendemail);


            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("ResetPassw")]
        public ApiTransferData<User> ResetPassw([FromBody] User resetPass)
        {
            ApiTransferData<User> returnValue = new ApiTransferData<User>();
            try
            {
                //String Email, String Token, Int32 Id, String Password
                // verificando se o token é valido 
                SecurityTokenBO securityTokenBO = new SecurityTokenBO();
                securityTokenBO.ValidateToken(resetPass.Token);

                //fazendo o select do usuario para pegar o id
                UserBO userBO = new UserBO();
                User userReset = userBO.SelectByEmail(resetPass.Email);
                if (userReset.Id != resetPass.Id) 
                {
                    throw new Exception("Id User Invalid");
                }
                userReset.Password = resetPass.Password;
                userReset.IsBlocked = "N";
                userReset.P_operation = "S";

                //fazendo o save com a senha certa
                User user = userBO.Save(userReset);



                // build return
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Password reset has successfully";
                returnValue.ReturnData = null;
                returnValue.ReturnValue = null;
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

    }


}