﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using MBLabs.TrackingLibSqlServer.Business;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/averbacaounidade")]
    public class HistoricoController : _BaseApiController
    {
        [HttpPost]
        [Route("Historico")]
        public ApiTransferData<Historico> InsertHistorico([FromBody] Historico historico)
        {
            ApiTransferData<Historico> returnValue = new ApiTransferData<Historico>();

            try
            {
                
                HistoricoBO historicoBO = new HistoricoBO();
                historicoBO.InsertHist(historico.EV_ID, historico.USU_ID_OPERADOR, historico.USU_ID_AFETADO, historico.hs_obs, historico.acesso_id);

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Insert Succesufully";
                returnValue.ReturnData = null;
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
    }


}
    