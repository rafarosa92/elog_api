﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using MBLabs.TrackingLibSqlServer.Business;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Web.Http;


namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/SigVig")]
    public class SigVigController : _BaseApiController
    {
        [HttpPost]
        [Route("GetBusca")]

        public ApiTransferData<IList<SigVig>>PreFiltroSigVig([FromBody] SigVig sigVig)
        {
            ApiTransferData<IList<SigVig>> returnValue = new ApiTransferData<IList<SigVig>>();
            try
            {
                SigVigBO sigVigBO = new SigVigBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = sigVigBO.GetBusca(sigVig);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("VerificaPosData")]

        public ApiTransferData<IList<SigVig>> VerificaPosData([FromBody] SigVig sigVig)
        {
            ApiTransferData<IList<SigVig>> returnValue = new ApiTransferData<IList<SigVig>>();
            try
            {
                SigVigBO sigVigBO = new SigVigBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = sigVigBO.VerificaPosData(sigVig);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("VerificaExcecao")]

        public ApiTransferData<IList<SigVig>> VerificaExcecao([FromBody] SigVig sigVig)
        {
            ApiTransferData<IList<SigVig>> returnValue = new ApiTransferData<IList<SigVig>>();
            try
            {
                SigVigBO sigVigBO = new SigVigBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = sigVigBO.VerificaExcecao(sigVig);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("SelectQtdPadrao")]

        public ApiTransferData<IList<SigVig>> SelectQtdPadrao([FromBody] SigVig sigVig)
        {
            ApiTransferData<IList<SigVig>> returnValue = new ApiTransferData<IList<SigVig>>();
            try
            {
                SigVigBO sigVigBO = new SigVigBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = sigVigBO.SelectQtdPadrao(sigVig);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("SelectQtdMaxDia")]

        public ApiTransferData<IList<SigVig>> SelectQtdMaxDia([FromBody] SigVig sigVig)
        {
            ApiTransferData<IList<SigVig>> returnValue = new ApiTransferData<IList<SigVig>>();
            try
            {
                SigVigBO sigVigBO = new SigVigBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = sigVigBO.SelectQtdMaxDia(sigVig);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("ProcAgenda")]

        public ApiTransferData<IList<SigVig>> ProcAgenda([FromBody] SigVig sigVig)
        {
            ApiTransferData<IList<SigVig>> returnValue = new ApiTransferData<IList<SigVig>>();
            try
            {
                SigVigBO sigVigBO = new SigVigBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = sigVigBO.ProcAgenda(sigVig);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("ProcMapaSara")]

        public ApiTransferData<IList<SigVig>> ProcMapaSara([FromBody] SigVig sigVig)
        {
            ApiTransferData<IList<SigVig>> returnValue = new ApiTransferData<IList<SigVig>>();
            try
            {
                SigVigBO sigVigBO = new SigVigBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = sigVigBO.ProcMapaSara(sigVig);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
    }
}