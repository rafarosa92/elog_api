﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/company")]
    public class CompanyController : _BaseApiController
    {
        [HttpGet]
        [Route("getbynullstatus")]

        public ApiTransferData<IList<Company>> GetByNullStatus()
        {
            ApiTransferData<IList<Company>> returnValue = new ApiTransferData<IList<Company>>();

            try
            {
                CompanyBO companyBO = new CompanyBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = companyBO.GetCompanyList();
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpGet]
        [Route("getByType")]

        public ApiTransferData<IList<Company>> GetByType(String type)
        {
            ApiTransferData<IList<Company>> returnValue = new ApiTransferData<IList<Company>>();
            
            try
            {
                CompanyBO companyBO = new CompanyBO();

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = companyBO.GetCompanyByType(type);
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpGet]
        [Route("getByLogin")]
        [UserAuthentication]
        public ApiTransferData<IList<Company>> GetByLogin(String Username)
        {
            ApiTransferData<IList<Company>> returnValue = new ApiTransferData<IList<Company>>();

            try
            {
                UserBO userBO = new UserBO();
                User user = userBO.SelectByLogin(Username);

                CompanyBO companyBO = new CompanyBO();

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = companyBO.GetCompanyByLogin(user.Id);
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpGet]
        [Route("getByLogin")]
        
        public ApiTransferData<IList<Company>> GetById(Int32 Id)
        {
            ApiTransferData<IList<Company>> returnValue = new ApiTransferData<IList<Company>>();

            try
            {

                CompanyBO companyBO = new CompanyBO();

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = companyBO.GetCompanyById(Id);
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
    }

}