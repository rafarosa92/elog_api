﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/menu")]
    public class MenuController : _BaseApiController
    {
        [HttpGet]
        [Route("MenuSelect")]
        [UserAuthentication]
        public ApiTransferData<IList<Menu>> MenuSelect(String Username)
        {
            ApiTransferData<IList<Menu>> returnValue = new ApiTransferData<IList<Menu>>();

            try
            {
                UserBO userBO = new UserBO();
                User user = userBO.SelectByLogin(Username);

           

                MenuBO menuBO = new MenuBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = menuBO.getMenuById(user.Id);
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
    }
}