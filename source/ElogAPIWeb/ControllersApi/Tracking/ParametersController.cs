﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/parameters")]
    public class ParametersController : _BaseApiController
    {
        [HttpGet]
        [Route("getParameters")]

        public ApiTransferData<IList<Parameters>> GetAllParameters()
        {
            ApiTransferData<IList<Parameters>> returnValue = new ApiTransferData<IList<Parameters>>();

            try
            {
                ParametersBO parametersBO = new ParametersBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = parametersBO.GetParametersList();
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpGet]
        [Route("getByParamDesc")]

        public ApiTransferData<IList<Parameters>> getByParamDesc(String parameters)
        {
            ApiTransferData<IList<Parameters>> returnValue = new ApiTransferData<IList<Parameters>>();

            try
            {
                ParametersBO parametersBO = new ParametersBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = parametersBO.GetParamByDesc(parameters);
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpGet]
        [Route("getByParamDesc")]

        public ApiTransferData<IList<ParametersCnpj>> getCnpjParam(Int32 Id)
        {
            ApiTransferData<IList<ParametersCnpj>> returnValue = new ApiTransferData<IList<ParametersCnpj>>();

            
            try
            {
                ParametersBO parametersBO = new ParametersBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = parametersBO.GetParamCnpjById(Id);
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
    }
}