﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using MBLabs.TrackingLibSqlServer.Business;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Web.Http;


namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/Extrato")]
    public class ExtratoController : _BaseApiController
    {
        [HttpPost]
        [Route("GetDadosLote")]

        public ApiTransferData<IList<Extrato>>GetDadosLote([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosLote(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetDadosCESVEntrada")]

        public ApiTransferData<IList<Extrato>> GetDadosCESVEntrada([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosCESVEntrada(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetDadosDocEntrada")]

        public ApiTransferData<IList<Extrato>> GetDadosDocEntrada([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosDocEntrada(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetVolRecebidos")]

        public ApiTransferData<IList<Extrato>> GetVolRecebidos([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosVolRecebidos(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpPost]
        [Route("GetVolRecebidosPendentes")]

        public ApiTransferData<IList<Extrato>> GetVolRecebidosPendentes([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosVolRecebidosPendentes(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetConteiner")]

        public ApiTransferData<IList<Extrato>> GetConteiner([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosConteiner(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpPost]
        [Route("GetAvariaConteiner")]

        public ApiTransferData<IList<Extrato>> GetAvariaConteiner([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetAvariaConteiner(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetAvariaLote")]

        public ApiTransferData<IList<Extrato>> GetAvariaLote([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosAvariaLote(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpPost]
        [Route("GetLacreConteiner")]

        public ApiTransferData<IList<Extrato>> GetLacreConteiner([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetLacreConteiner(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("PreFiltro")]
        public ApiTransferData<IList<Extrato>> PreFiltroExtrato([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                ParametersBO parametersBO = new ParametersBO();
                CompanyBO companyBO = new CompanyBO();

                extrato.uni_id = companyBO.GetCompanyByName(extrato.CompanyName).Id;
                extrato.BeneficiarioCNPJ = parametersBO.SelectCNPJs(extrato.usu_id).beneficiario;
                extrato.ComissariaCNPJ = parametersBO.SelectCNPJs(extrato.usu_id).comissaria;
                if (extrato.acesso60)
                {
                    extrato.CNPJs = String.Empty;
                    extrato.Tipo = "B";
                }
                else
                {
                    if (!String.IsNullOrEmpty(extrato.BeneficiarioCNPJ))
                    {
                        extrato.CNPJs = extrato.BeneficiarioCNPJ;
                        extrato.Tipo = "B";
                    }
                    else
                    {
                        extrato.CNPJs = extrato.ComissariaCNPJ;
                        extrato.Tipo = "C";
                    }
                }

                
                extrato.TPU_VALOR = parametersBO.GetByParamDescUni("RETROACAO_EXTRATO", extrato.uni_id).TPU_VALOR;

                

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetProcPreFiltro(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpPost]
        [Route("GetDadosDocSaida")]

        public ApiTransferData<IList<Extrato>> GetDadosDocSaida([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosDocSaida(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpPost]
        [Route("GetDadosCESVSaida")]

        public ApiTransferData<IList<Extrato>> GetDadosCESVSaida([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosCESVSaida(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetDadosFaturamento")]

        public ApiTransferData<IList<Extrato>> GetDadosFaturamento([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosFaturamento(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetDadosFaturamento")]

        public ApiTransferData<IList<Extrato>> GetDadosDaCda([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosDaCda(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetDadosFaturamento")]

        public ApiTransferData<IList<Extrato>> GetDadosApreensao([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetDadosApreensao(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetProcPeso")]

        public ApiTransferData<IList<Extrato>> GetProcPeso([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = extratoBO.GetProcPeso(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetProcPesoAferido")]

        public ApiTransferData<IList<Extrato>> GetProcPesoAferido([FromBody] Extrato extrato)
        {
            ApiTransferData<IList<Extrato>> returnValue = new ApiTransferData<IList<Extrato>>();
            try
            {
                ExtratoBO extratoBO = new ExtratoBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was GetProcPesoAferido";
                returnValue.ReturnData = extratoBO.GetProcPesoAferido(extrato);
            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
    }
}