﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLibSqlServer.Business;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/averbacaounidade")]
    public class AverbacaoUnidadeController : _BaseApiController
    {
        [HttpPost]
        [Route("Averbacao")]
        public ApiTransferData<AverbacaoUnidade> SelectByName([FromBody] AverbacaoUnidade averbacaoUnidade)
        {
            ApiTransferData<AverbacaoUnidade> returnValue = new ApiTransferData<AverbacaoUnidade>();

            try
            {
                AverbacaoUnidadeBO averbacaoUnidadeBO = new AverbacaoUnidadeBO();
                CompanyBO companyBO = new CompanyBO();
                averbacaoUnidade.Uni_ID = companyBO.GetCompanyByName(averbacaoUnidade.CompanyName).UNI_COD1;
      
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Insert Succesufully";
                returnValue.ReturnData = averbacaoUnidadeBO.GetById(averbacaoUnidade.Uni_ID);
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
    }


}
    