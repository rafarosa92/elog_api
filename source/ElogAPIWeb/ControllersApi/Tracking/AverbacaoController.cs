﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Mechanism;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLibSqlServer.Business;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Http;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/averbacao")]
    public class AverbacaoController : _BaseApiController
    {
      
        [HttpPost]
        [Route("Averbacao")]
        public ApiTransferData<Averbacao> Insert([FromBody] Averbacao averbacao)
        {
            ApiTransferData<Averbacao> returnValue = new ApiTransferData<Averbacao>();

            try
            {
                AverbacaoBO averbacaoBO = new AverbacaoBO();
                CompanyBO companyBO = new CompanyBO();
                averbacao.Uni_ID = companyBO.GetCompanyByName(averbacao.CompanyName).UNI_COD1;

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Insert Succesufully";
                returnValue.ReturnData = averbacaoBO.SaveDI(averbacao);

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpPost]
        [Route("AverbacaoByDate")]
        public ApiTransferData<IList<Averbacao>> GetByDate([FromBody] SelectByDate dateAverbacaoSelect)
        {
            ApiTransferData<IList<Averbacao>> returnValue = new ApiTransferData<IList<Averbacao>>();
            returnValue.ReturnValue = ConfigMapper.BancoUsado;

            try
            {
                CompanyBO companyBO = new CompanyBO();
                dateAverbacaoSelect.Uni_ID = companyBO.GetCompanyByName(dateAverbacaoSelect.CompanyName).UNI_COD1;
                AverbacaoBO averbacaoBO = new AverbacaoBO();

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = averbacaoBO.GetAverbacaoByDate(dateAverbacaoSelect);
                
                 
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("AlteraResponsavel")]
        public ApiTransferData<Averbacao> AlteraResponsavel([FromBody] Averbacao averbacao)
        {
            ApiTransferData<Averbacao> returnValue = new ApiTransferData<Averbacao> ();

            try
            {
                
                
                AverbacaoBO averbacaoBO = new AverbacaoBO();
                averbacaoBO.AlteraResponsavel(averbacao);

                LogBO logBO = new LogBO();
                Log log = new Log();
                log.DI_ID = averbacao.DI_ID;
                log.Le_UCriacao = averbacao.DI_UCriacao;
                log.Le_Descricao = new LogMsg().MsgResponsavel+averbacao.Di_Responsavel;
                logBO.SaveLog(log);

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Novo Responsável Atribuido!";
                returnValue.ReturnData = null;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("CancelarDI")]
        public ApiTransferData<Averbacao> CancelarDI([FromBody] Averbacao averbacao)
        {
            ApiTransferData<Averbacao> returnValue = new ApiTransferData<Averbacao>();

            try
            {


                AverbacaoBO averbacaoBO = new AverbacaoBO();
                averbacaoBO.CancelarDI(averbacao);

                LogBO logBO = new LogBO();
                Log log = new Log();
                log.DI_ID = averbacao.DI_ID;
                log.Le_UCriacao = averbacao.DI_UCriacao;
                log.Le_Descricao = new LogMsg().MsgCancelamento;
                logBO.SaveLog(log);

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "DI Cancelada com Sucesso!";
                returnValue.ReturnData = null;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("CancelarDI")]
        public ApiTransferData<Averbacao> ReprocessarDI([FromBody] Averbacao averbacao)
        {
            ApiTransferData<Averbacao> returnValue = new ApiTransferData<Averbacao>();

            try
            {


                AverbacaoBO averbacaoBO = new AverbacaoBO();
                averbacaoBO.ReprocessarDI(averbacao);

                LogBO logBO = new LogBO();
                Log log = new Log();
                log.DI_ID = averbacao.DI_ID;
                log.Le_UCriacao = averbacao.DI_UCriacao;
                log.Le_Descricao = new LogMsg().MsgReprocessamento+averbacao.DI_DtCriacao;
                logBO.SaveLog(log);

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "DI Reprocessada com Sucesso!";
                returnValue.ReturnData = null;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("RevisarDI")]
        public ApiTransferData<Averbacao> RevisarDI([FromBody] Averbacao averbacao)
        {
            ApiTransferData<Averbacao> returnValue = new ApiTransferData<Averbacao>();

            try
            {
                String Numero_DI = averbacao.DI_Numero;

      
                UploadBO uploadBO = new UploadBO();
                uploadBO.GetLogDocByDiNumero(averbacao.DI_Numero);
                IList<Upload> log_doc =  uploadBO.GetLogDocByDiNumero(Numero_DI);

                CompanyBO companyBO = new CompanyBO();
                String destinatario = companyBO.GetCompanyByName(averbacao.CompanyName).KeeperEmail;

                LogBO logBO = new LogBO();
                Log log = new Log();
                log.DI_ID = averbacao.DI_ID;
                log.Le_UCriacao = averbacao.DI_UCriacao;
                log.Le_Descricao = new LogMsg().MsgRevisao;
                logBO.SaveLog(log);
                
                String email = new StreamReader(@"C:\inetpub\elog_api\Views\emails\RevisaoDI.html").ReadToEnd();
                String lista = string.Empty;
                for (int i = 0; i < log_doc.Count; i++) {
                    String Ld_NomeDocumento = log_doc[i].Ld_NomeDocumento;
                    String DateCreate = log_doc[i].DateCreate.ToString();
                    lista += "<p>Documento: " + Ld_NomeDocumento+" - Data de Criação: "+ DateCreate+"</p>";
                }
                email = email.Replace("#LOG_DOC#", lista);
                email = email.Replace("#USUARIO#", averbacao.DI_UCriacao);
                email = email.Replace("#RESPONSAVEL#", averbacao.Di_Responsavel);


                EmailSender.SendEmail(destinatario, "rafael@sidedoor.com.br", "Solicitação de Revisão - E-Tracking", email);

                //AverbacaoBO averbacaoBO = new AverbacaoBO();
               // averbacaoBO.CancelarDI(averbacao);
                //returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Revisão com Sucesso!";
                returnValue.ReturnData = null;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetLog")]
        public ApiTransferData<IList<Log>> GetLog([FromBody] Averbacao averbacao)
        {
            ApiTransferData<IList<Log>> returnValue = new ApiTransferData<IList<Log>>();
            
            try
            {

                LogBO logBO = new LogBO();
                Log log = new Log();
                log.DI_ID = averbacao.DI_ID;

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select com Sucesso!";
                returnValue.ReturnData = logBO.GetLogByID(log);
                returnValue.ReturnValue = ConfigMapper.BancoUsado;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
                returnValue.ReturnValue = ConfigMapper.BancoUsado;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetLog")]
        public ApiTransferData<IList<Log>> GetPendencias([FromBody] Averbacao averbacao)
        {
            ApiTransferData<IList<Log>> returnValue = new ApiTransferData<IList<Log>>();

            try
            {

                LogBO logBO = new LogBO();
                Log log = new Log();
                log.pendencia = averbacao.DI_SDSAI_ID;
                log.UniPendencia = averbacao.CompanyName;

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select com Sucesso!";
                returnValue.ReturnData = logBO.GetPendencia(log);
                returnValue.ReturnValue = ConfigMapper.BancoUsado;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
                returnValue.ReturnValue = ConfigMapper.BancoUsado;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetDadosLote")]
        public ApiTransferData<IList<Comprovante>> GetDadosLote([FromBody] Averbacao averbacao)
        {
            ApiTransferData<IList<Comprovante>> returnValue = new ApiTransferData<IList<Comprovante>>();

            try
            {

                ComprovanteBO comprovanteBO = new ComprovanteBO();
                Comprovante comprovante = new Comprovante();

                comprovante.DI_SLOTE_ID = averbacao.DI_SLOTE_ID;
                comprovante.CompanyName = averbacao.CompanyName;
    

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select com Sucesso!";
                returnValue.ReturnData = comprovanteBO.GetDadosLote(comprovante);
                returnValue.ReturnValue = ConfigMapper.BancoUsado;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
                returnValue.ReturnValue = ConfigMapper.BancoUsado;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("getDados")]
        public ApiTransferData<IList<Averbacao>> GetDadosDI([FromBody] Averbacao averbacao)
        {
            ApiTransferData<IList<Averbacao>> returnValue = new ApiTransferData<IList<Averbacao>>();

            try
            {
        
                AverbacaoBO averbacaoBO = new AverbacaoBO();

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = averbacaoBO.GetDadosDI(averbacao);
                returnValue.ReturnValue = ConfigMapper.BancoUsado;


            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpPost]
        [Route("getLocalizacao")]
        public ApiTransferData<IList<Localizacao>> GetLocalizacaoDI([FromBody] Averbacao averbacao)
        {
            ApiTransferData<IList<Localizacao>> returnValue = new ApiTransferData<IList<Localizacao>>();

            try
            {


                LocalizacaoBO localizacaoBO = new LocalizacaoBO();
                Localizacao localizacao = new Localizacao();

                localizacao.DI_SLOTE_ID = averbacao.DI_SLOTE_ID;
                localizacao.CompanyName = averbacao.CompanyName;

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = localizacaoBO.GetDadosLocalizacao(localizacao);
                returnValue.ReturnValue = ConfigMapper.BancoUsado;


            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpPost]
        [Route("GetUniInfo")]
        public ApiTransferData<IList<InfoUnidade>> GetUniInfo([FromBody] Averbacao averbacao)
        {
            ApiTransferData<IList<InfoUnidade>> returnValue = new ApiTransferData<IList<InfoUnidade>>();

            try
            {


                InfoUnidadeBO infoUnidadeBO = new InfoUnidadeBO();
                InfoUnidade infoUnidade = new InfoUnidade();

                infoUnidade.CompanyName = averbacao.CompanyName;


                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = infoUnidadeBO.GetDadosUniInfo(infoUnidade);
                returnValue.ReturnValue = ConfigMapper.BancoUsado;


            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetPesoAferido")]
        public ApiTransferData<IList<Comprovante>> GetPesoAferido([FromBody] Averbacao averbacao)
        {
            ApiTransferData<IList<Comprovante>> returnValue = new ApiTransferData<IList<Comprovante>>();

            try
            {

                ComprovanteBO comprovanteBO = new ComprovanteBO();
                Comprovante comprovante = new Comprovante();

                comprovante.DI_SLOTE_ID = averbacao.DI_SLOTE_ID;
                comprovante.CompanyName = averbacao.CompanyName;


                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select com Sucesso!";
                returnValue.ReturnData = comprovanteBO.GetPesoAferido(comprovante);
                returnValue.ReturnValue = ConfigMapper.BancoUsado;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
                returnValue.ReturnValue = ConfigMapper.BancoUsado;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetEspecie")]
        public ApiTransferData<IList<Comprovante>> GetEspecie([FromBody] Averbacao averbacao)
        {
            ApiTransferData<IList<Comprovante>> returnValue = new ApiTransferData<IList<Comprovante>>();

            try
            {

                ComprovanteBO comprovanteBO = new ComprovanteBO();
                Comprovante comprovante = new Comprovante();

                comprovante.DI_SLOTE_ID = averbacao.DI_SLOTE_ID;
                comprovante.CompanyName = averbacao.CompanyName;


                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select com Sucesso!";
                returnValue.ReturnData = comprovanteBO.GetEspecie(comprovante);
                returnValue.ReturnValue = ConfigMapper.BancoUsado;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
                returnValue.ReturnValue = ConfigMapper.BancoUsado;
            }

            return returnValue;
        }

        [HttpPost]
        [Route("GetEspecie")]
        public ApiTransferData<IList<Comprovante>> GetAvaria([FromBody] Comprovante comprovante)
        {
            ApiTransferData<IList<Comprovante>> returnValue = new ApiTransferData<IList<Comprovante>>();

            try
            {

                ComprovanteBO comprovanteBO = new ComprovanteBO();

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select com Sucesso!";
                returnValue.ReturnData = comprovanteBO.GetAvaria(comprovante);
                returnValue.ReturnValue = ConfigMapper.BancoUsado;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
                returnValue.ReturnValue = ConfigMapper.BancoUsado;
            }

            return returnValue;
        }
        [HttpPost]
        [Route("GetEspecie")]
        public ApiTransferData<IList<AverbacaoUnidade>> GetParamComprovante([FromBody] Comprovante comprovante)
        {
            ApiTransferData<IList<AverbacaoUnidade>> returnValue = new ApiTransferData<IList<AverbacaoUnidade>>();

            try
            {
                CompanyBO companyBO = new CompanyBO();
                AverbacaoUnidadeBO averbacaoUnidadeBO = new AverbacaoUnidadeBO();
                Int32 uni_code = companyBO.GetCompanyByName(comprovante.CompanyName).UNI_COD1;

                

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select com Sucesso!";
                returnValue.ReturnData = averbacaoUnidadeBO.GetParamComprovante(uni_code);
                returnValue.ReturnValue = ConfigMapper.BancoUsado;

            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
                returnValue.ReturnValue = ConfigMapper.BancoUsado;
            }

            return returnValue;
        }


    }



}
    