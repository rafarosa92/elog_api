﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Mechanism;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.tr
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/Averbacao_DI")]
    public class Averbacao_DIController : _BaseApiController
    {
        [HttpGet]
        [Route("Averbacao_DI")]

        public ApiTransferData<IList<Averbacao_DI>> GetAverbacao_DI()
        {
            ApiTransferData<IList<Averbacao_DI>> returnValue = new ApiTransferData<IList<Averbacao_DI>>();

            try
            {
                FileUploader.Upload();
                Averbacao_DIBO averbacao_DIBO = new Averbacao_DIBO();
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = averbacao_DIBO.GetAverbacaoList();
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }


    }

}