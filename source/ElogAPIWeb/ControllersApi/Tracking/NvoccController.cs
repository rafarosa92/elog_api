﻿using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using MBLabs.TrackingLibSqlServer.Business;
using MBLabs.TrackingLibSqlServer.Transfer;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/Nvocc")]
    public class NvoccController : _BaseApiController
    {
        [HttpPost]
        [Route("GetNvocc")]

        public ApiTransferData<IList<Nvocc>>GetNvocc([FromBody] Nvocc nvocc)
        {
            ApiTransferData<IList<Nvocc>> returnValue = new ApiTransferData<IList<Nvocc>>();
            
            ParametersBO parametersBO = new ParametersBO();
            NvoccBO nvoccBO = new NvoccBO();
            nvocc.cli_cnpj = parametersBO.SelectConsolidadorCNPJ(nvocc.usu_id).CnpjCliente;
            try
            {
                
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = nvoccBO.ProcBuscaLotes(nvocc);
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }

        [HttpPost]
        [Route("BloqDesbloq")]

        public ApiTransferData<IList<Nvocc>> BloqDesbloq([FromBody] Nvocc nvocc)
        {
            ApiTransferData<IList<Nvocc>> returnValue = new ApiTransferData<IList<Nvocc>>();

            
            NvoccBO nvoccBO = new NvoccBO();
            
            try
            {

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Select was sucessfully";
                returnValue.ReturnData = nvoccBO.ProcBloqDesbloq(nvocc);
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }


    }
}