﻿
using MBLabs.ElogAPILib.Mechanism;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.ElogAPIWeb.Filters;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.IO;
using MBLabs.TrackingLib.Enumerator;
using System.Net;
using MBLabs.TrackingLibSqlServer.Transfer;
using MBLabs.TrackingLibSqlServer.Business;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/tracking")]
    public class UploadController : _BaseApiController
    {
        
        [HttpPost]
        [Route("Upload")]
        public ApiTransferData<Upload> UploadFile([FromBody] Upload upload)
        {
            ApiTransferData<Upload> returnValue = new ApiTransferData<Upload>();

            try
            {
                CompanyBO companyBO = new CompanyBO();
                upload.Uni_ID = companyBO.GetCompanyByName(upload.CompanyName).UNI_COD1;

                AverbacaoUnidadeBO averbacaoUnidadeBO = new AverbacaoUnidadeBO();
                upload.PathFile = averbacaoUnidadeBO.GetPathFile(upload.Uni_ID).Uni_CaminhoArq;

                UploadBO uploadBo = new UploadBO();
                upload = uploadBo.SaveFile(upload);

                returnValue.ReturnValue = upload.NameFile;
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "File Save Success";
                returnValue.ReturnData = upload;

            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }
            return returnValue;
        }
        [HttpPost]
        [Route("Upload")]
        public ApiTransferData<IList<Upload>> DownloadFile([FromBody] Upload upload)
        {
            ApiTransferData<IList<Upload>> returnValue = new ApiTransferData<IList<Upload>>();

            try
            {
                CompanyBO companyBO = new CompanyBO();
                upload.Uni_ID = companyBO.GetCompanyByName(upload.CompanyName).UNI_COD1;

                AverbacaoUnidadeBO averbacaoUnidadeBO = new AverbacaoUnidadeBO();
                upload.PathFile = averbacaoUnidadeBO.GetPathFile(upload.Uni_ID).Uni_CaminhoArq;

                UploadBO uploadBo = new UploadBO();
      
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "File Download Success";
                returnValue.ReturnData = uploadBo.DownloadHash(upload);

            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }
            return returnValue;
        }
        [HttpPost]
        [Route("Delete")]
        public ApiTransferData<Upload> DeleteFile([FromBody] Upload upload)
        {
            ApiTransferData<Upload> returnValue = new ApiTransferData<Upload>();

            try
            {
                CompanyBO companyBO = new CompanyBO();
                upload.Uni_ID = companyBO.GetCompanyByName(upload.CompanyName).UNI_COD1;

                AverbacaoUnidadeBO averbacaoUnidadeBO = new AverbacaoUnidadeBO();
                upload.PathFile = averbacaoUnidadeBO.GetPathFile(upload.Uni_ID).Uni_CaminhoArq;

                UploadBO uploadBo = new UploadBO();

                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "File Delete Success";
                returnValue.ReturnData = uploadBo.DeleteFile(upload);

            }
            catch (Exception ex)
            {
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }
            return returnValue;
        }
    }
}