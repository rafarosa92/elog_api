﻿using log4net;
using MBLabs.ElogAPILib.Business;
using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Transfer;
using MBLabs.TrackingLib.Business;
using MBLabs.TrackingLib.Transfer;
using System;
using System.Web.Http;

namespace MBLabs.ElogAPIWeb.ControllersApi
{
    [Route("api/securitytoken")]
    public class SecurityTokenController : _BaseApiController
    {
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [HttpPost]
        [Route("generatetoken")]
        public ApiTransferData<SecurityToken> GenerateToken([FromBody] GenerateToken form)
        {
            ApiTransferData<SecurityToken> returnValue = new ApiTransferData<SecurityToken>();
            try
            {
                // genarete token authenticate
                SecurityTokenBO securityTokenBO = new SecurityTokenBO();
                SecurityToken securityToken = securityTokenBO.Authenticate(base.GetHeader().APIKey, form.Username, form.Password, base.GetClientIP());
                new HistoricoBO().InsertHist(4, securityToken.InsertByUserId, securityToken.InsertByUserId, "LOGANDO NO E-TRACKING NOVO", 1);
                
                // build return
                returnValue.ResultStatus = ResultStatus.SUCCESS.ToString();
                returnValue.ResultStatusMessage = "Token was generete successfully";
                returnValue.ReturnData = securityToken;
            }
            catch (Exception ex)
            {
                // invalid credentials
                returnValue.ResultStatus = ResultStatus.ERROR.ToString();
                returnValue.ResultStatusMessage = String.Format(ex.Message);
                returnValue.ReturnData = null;
            }

            return returnValue;
        }
    }

    public class GenerateToken
    {
        public String Username { get; set; }
        public String Password { get; set; }
    }
}