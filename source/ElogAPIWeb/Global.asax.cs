﻿using FluentScheduler;
using log4net;
using log4net.Config;
using MBLabs.ElogAPIWeb.Jobs;
using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MBLabs.ElogAPIWeb
{
    public class MvcApplication : System.Web.HttpApplication
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        protected void Application_Start()
        {
            // Register all necessary cfgs
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.EnsureInitialized();

            // log4net configuration
            XmlConfigurator.Configure(new Uri(AppDomain.CurrentDomain.BaseDirectory + "Web.config"));

            TaskManager.Initialize(new TaskManagerJobs());
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            _log.Error(ex);
            Server.ClearError();
        }
    }
}