using MBLabs.ElogAPIWeb.Controllers;
namespace MBLabs.ElogAPIWeb.Models
{
	public interface IModelConverter<T1, T2>
	{
		T2 ConvertToModel(T1 model);
	}
}