﻿using System.Collections.Generic;
using MBLabs.ElogAPILib.Transfer.SearchParameter;
using MBLabs.ElogAPILib.Transfer;
using System;

namespace MBLabs.ElogAPIWeb.Models
{
    public  class _BaseListModel<T1,T2> where T2 : BaseSearchParameter
    {
        public T2 SearchParameter { get; set; }
        public IList<T1> ModelList { get; set; }
        public Int32 TotalRows { get; set; }
    }
}