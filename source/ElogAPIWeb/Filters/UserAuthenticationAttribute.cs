﻿using log4net;
using MBLabs.ElogAPILib.Business;
using MBLabs.ElogAPILib.Enumerator;
using MBLabs.ElogAPILib.Transfer;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace MBLabs.ElogAPIWeb.Filters
{
    /// <summary>
    /// Attribute for security filter.
    /// </summary>
    public class UserAuthenticationAttribute : ActionFilterAttribute
    {
        #region Constants
        private static readonly ILog _log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        protected string GetClientIP(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            else if (HttpContext.Current != null)
            {
                return HttpContext.Current.Request.UserHostAddress;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Called before the action method executes and authenticate the user.
        /// If the user is not authenticated through authorization parameter in the header of the request, it gets a 401 unauthorized response.
        /// </summary>
        /// <param name="actionContext">Context information for the executing action</param>
        /// <seealso cref="http://en.wikipedia.org/wiki/Basic_access_authentication"/>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                // check if request header contains authorization value
                if (actionContext.Request.Headers.Authorization != null)
                {
                    // get token auth from request header
                    String headerAuthorization = actionContext.Request.Headers.Authorization.Parameter;
                    String tokenAuth = headerAuthorization.Substring(headerAuthorization.IndexOf(" ", StringComparison.Ordinal) + 1).Replace("token=", String.Empty).Replace("\"", String.Empty);
                    String apiToken = actionContext.Request.Headers.GetValues("api_key").FirstOrDefault();
                    String requestIP = this.GetClientIP(actionContext.Request);
                    String requestUrl = actionContext.Request.RequestUri.AbsolutePath;
                    String action = actionContext.ActionDescriptor.ActionName;
                    String controller = actionContext.ActionDescriptor.ControllerDescriptor.ControllerName;

                    // check token exist and approved
                    SecurityTokenBO securityTokenBO = new SecurityTokenBO();
                    securityTokenBO.ValidateToken(tokenAuth);

                    // security mapping table
                    SecuritySystemBO securitySystemBO = new SecuritySystemBO();
                    Boolean canAccess = securitySystemBO.ValidateAccess(requestIP, apiToken, requestUrl);

                    if (canAccess)
                    {
                        JObject jsonObject = new JObject();
                        jsonObject.Add("ResultStatus", ResultStatus.SUCCESS.ToString());
                        jsonObject.Add("ResultStatusMessage", "User authenticated successfully");
                        jsonObject.Add("ReturnData", "");
                    }
                    else
                    {
                        JObject jsonObject = new JObject();
                        jsonObject.Add("ResultStatus", ResultStatus.ERROR.ToString());
                        jsonObject.Add("ResultStatusMessage", "MappingTable blocked the access from " + requestIP);
                        jsonObject.Add("ReturnData", "");
                        actionContext.Response = actionContext.Request.CreateResponse<JObject>(HttpStatusCode.Unauthorized, jsonObject);
                        return;
                    }
                }
                else
                {
                    JObject jsonObject = new JObject();
                    jsonObject.Add("ResultStatus", ResultStatus.ERROR.ToString());
                    jsonObject.Add("ResultStatusMessage", "Authorization not found");
                    jsonObject.Add("ReturnData", "");
                    actionContext.Response = actionContext.Request.CreateResponse<JObject>(HttpStatusCode.Unauthorized, jsonObject);
                    return;
                }
            }
            catch (Exception ex)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                JObject jsonObject = new JObject();
                jsonObject.Add("ResultStatus", ResultStatus.ERROR.ToString());
                jsonObject.Add("ResultStatusMessage", ex.Message.ToString());
                jsonObject.Add("ReturnData", "");
                actionContext.Response = actionContext.Request.CreateResponse<JObject>(HttpStatusCode.Unauthorized, jsonObject);
                return;
            }

            base.OnActionExecuting(actionContext);
        }
    }
}